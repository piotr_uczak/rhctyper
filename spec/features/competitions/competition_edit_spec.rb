require 'rails_helper'
# Feature: Competition edit
#   As a competition owner
#   I want to edit my competition
#   So I can choose who participates in this competition

feature 'Competition edit' do
    scenario 'user adds a participant to the competition' do
        user = FactoryBot.create(:user)
        event = FactoryBot.create(:event)
        competition = FactoryBot.create(:competition, event: event)
        login_as(user, :scope => :user)
        visit edit_competition_path(competition)
        #expect(page).to have_select('usr_select')
    end
end
