require 'rails_helper'
# Feature: Competition edit
#   As an admin
#   I want to edit score of the game

feature 'Game edit' do
    context 'Logged in user who is an admin' do
        before :each do
            admin = FactoryBot.create(:user, :admin)
            login_as(admin, :scope => :user)
        end

        scenario 'admin updates the score of the game' do
            game = FactoryBot.create(:game)
            visit edit_game_path(game)
            expect(page).to have_field('game_away_score')
            expect(page).to have_field('game_home_score')
            fill_in 'game_home_score', with: '3'
            fill_in 'game_away_score', with: '1'
            click_button 'Aktualizuj'
            expect(page).to have_content('Game was successfully updated')            
        end
    end

    context 'Logged in user who is not an admin' do
        before :each do
            user = FactoryBot.create(:user)
            login_as(user, :scope => :user)
        end
        
        scenario 'non-admin cannot see the game edit view' do            
            game = FactoryBot.create(:game)
            visit edit_game_path(game)
            expect(page).to have_content('You are not authorized to access this page')
        end
    end
end