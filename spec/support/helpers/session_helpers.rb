module Features
  module SessionHelpers
    def sign_up_with(email, password, confirmation)
      visit new_user_registration_path
      fill_in 'email', with: email
      fill_in 'password', with: password
      fill_in 'passwordconfirmation', :with => confirmation
      click_button 'signup'
    end

    def signin(email, password)
      visit root_path
      fill_in 'email', with: email
      fill_in 'password', with: password
      click_button 'Signin'
    end
  end
end
