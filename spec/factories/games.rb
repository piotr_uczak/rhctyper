FactoryBot.define do
  factory :game do
    transient do
      home_team_name {"Polska"}
      away_team_name {"Polska"}
    end
    
    home_team do
      Team.find_by(name: home_team_name) || FactoryBot.create(:team, name: home_team_name)
    end
    away_team do
      Team.find_by(name: away_team_name) || FactoryBot.create(:team, name: away_team_name)
    end

    start_date {"2015-06-22 22:35:36"}
    finish_date {"2015-06-22 22:35:36"}
    description {""}
  end
end
