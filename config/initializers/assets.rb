# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path

%w(stylesheets javascripts fonts images).each do |sub|
	Rails.application.config.assets.paths << Rails.root.join('app/assets',sub).to_s
end

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile << %r(MaterialDesignIcon\.(?:eot|svg|ttf|woff2?)$)
# Rails.application.config.assets.precompile << %r(font-awesome\.(?:eot|svg|ttf|woff2?)$)

