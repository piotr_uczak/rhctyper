Rails.application.routes.draw do
  namespace :admin do
      resources :users
      resources :bets
      resources :competitions
      resources :competitions_users
      resources :events
      resources :games
      resources :teams

      root to: "users#index"
    end
  resources :bets

  resources :competitions

  resources :events

  resources :games

  resources :teams

  get 'competitions/:id/results' => 'competitions#results', :as => :competition_results
  get 'competitions/:id/join' => 'competitions#join', :as => :competition_join
  post 'competitions/:id/add_user' => 'competitions#add_user', :as => :competition_add_user
  delete 'competitions/:id/remove_user' => 'competitions#remove_user', :as => :competition_remove_user
  get 'games/:id/all_bets/:idc' => 'games#all_bets', :as => :games_all_bets
  get 'competition/:id/all_winner_bets/:idc' => 'competitions#all_winner_bets', :as => :competition_all_winner_bets

authenticated :user do
    # Rails 4 users must specify the 'as' option to give it a unique name
    root :to => "visitors#index", :as => "authenticated_root"
end

devise_scope :user do
  root to: 'devise/sessions#new'
end
  devise_for :users	
  
  resources :users
end
