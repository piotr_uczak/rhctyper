// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require jquery_ujs
//= require jquery.countdown
//= require jquery.countdown-pl
// require material-sprockets
//= require materialize-sprockets

    var editBetHtml = '<div class="edit-bet"><div class="form-group bet-input-group"><input name="bet[home_score]" type="number" min="0" max="9" class="form-control bet-input"><input class="form-control bet-input bet-score-colon" value=":" class="bet-score-colon" disabled><input name="bet[away_score]" type="number" min="0" max="9" class="form-control bet-input"></div><div class="bet-error-container"><span class="bet-error form-help form-help-msg text-red"></span></div><div class="bet-actions"><a class="save-button">ZAPISZ</a></div></div>';
    var displayBetHtml = '<div class="display-bet"><div class="bet"></div><div class="edit-bet-icon"><i class="material-icons">edit</i></div></div>';
	var busyCircleHtml = '<div style="line-height: 110px; vertical-align: middle" class="preloader-wrapper small active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>';
	
	function moveGameToLive(selector) {
		var match = selector.clone();
		var section = selector.parents('.section');
		var displayAllBets = match.find('.display-all-bets-icon');
        var editBet = match.find('.edit-bet-icon');
        var form = match.find('form');
        var homeScore = form.find('input[name="prev-bet-home"]').val();
        var awayScore = form.find('input[name="prev-bet-away"]').val();
        var container = match.find('.bet-container');
		$('#live').prepend(match);
		container.displayBet();
		selector.remove();
		match.find('.game-time').html('TRWA');
		if (section.find('div').length == 0) {
			section.remove();
			section.next('.divider').remove();
		}
		
		editBet.hide();
        displayAllBets.show();
        
        if (!homeScore || !awayScore) {
        	var displayBet = container.find('.display-bet');
        	displayBet.empty();
        	displayBet.append('<div class="add-bet">Brak</div>');
        } 
	}
	
	function setIntervalAndExecute(fn, t) {
	    fn();
	    return(setInterval(fn, t));
	}

	$(document).ready(function() {
		$('select').material_select();
	});

    $.fn.editBet = function(event) {
    	var container = $(this);
    	$('.bet-container').each(function() {
            $(this).displayBet();
        });
        var form = $(this).parents('form');
        $('.fixed-action-btn').hide();
        container.empty();
        container.append(editBetHtml);
        var prevHomeScoreSelector = form.find('input[name="prev-bet-home"]');
        var prevAwayScoreSelector = form.find('input[name="prev-bet-away"]');
        var homeScoreSelector = form.find('input[name="bet[home_score]"]');
        var awayScoreSelector = form.find('input[name="bet[away_score]"]');
        var betErrorSelector = form.find('.bet-error');
        var betErrorContainer = form.find('.bet-error-container');
        var prevHomeScore = prevHomeScoreSelector.val();
        var prevAwayScore = prevAwayScoreSelector.val();
        var authToken = form.find('input[name="authenticity_token"]').val();
        var gameId = form.find('input[name="bet[game_id]"]').val();
        var competitionId = form.find('input[name="bet[competition_id]"]').val();
        var betId = form.find('input[name="bet_id"]').val();
        var url = '/bets';
        if (prevHomeScoreSelector.val() && prevAwayScoreSelector.val()) {
        	url += '/' + betId;
        	form.find('input[name="_method"]').val("patch");
        }
		var betActions = container.find('.bet-actions');
        homeScoreSelector.val(prevHomeScore);
        awayScoreSelector.val(prevAwayScore);
        container.find('.save-button').on("click touchend", function() {
            var newHomeScore = homeScoreSelector.val();
            var newAwayScore = awayScoreSelector.val();
            if (!$.isNumeric(newHomeScore) || newHomeScore < 0 || newHomeScore > 9) {
                container.find('.bet-input').css('border-bottom-color','#f44336');
                homeScoreSelector.css('color','#f44336');
                betErrorSelector.text("Zakres liczb: 0-9");
                return false;
            }
            if (!$.isNumeric(newAwayScore) || newAwayScore < 0 || newAwayScore > 9) {
                container.find('.bet-input').css('border-bottom-color','#f44336');
                awayScoreSelector.css('color','#f44336');
                betErrorSelector.text("Zakres liczb: 0-9");
                return false;
            }
            betActions.empty();
			  betActions.html(busyCircleHtml);
            $.ajax({
                type: 'post',
                url: url,
                data: form.serialize()
            }).done(function(bet) {
                var newHomeScore = homeScoreSelector.val();
                var newAwayScore = awayScoreSelector.val();
                prevHomeScoreSelector.val(newHomeScore);
                prevAwayScoreSelector.val(newAwayScore);
                if (betId.length == 0) {
                	form.find('input[name="bet_id"]').val(bet.id);
                }
                container.displayBet();
            }).fail(function() {
            	container.editBet();
            	container.find('.bet-input').css('border-bottom-color','#f44336');
                container.parents('form').find('input').css('color','#f44336');
                container.find('.bet-error').text("Błąd podczas zapisu");
            });
        });
        if (event) {
	        event.stopPropagation();
	        container.bind("click touchend", function(event) {
	        	event.stopPropagation(event);
	        });
        }
    };

    $.fn.displayBet = function() {
        var container = $(this);
        var form = $(this).parents('form');
        var startTime = new Date(parseInt($(this).parents('.match-row').find('input[name="game-time"]').val()));
        var homeScore = form.find('input[name="prev-bet-home"]').val();
        var awayScore = form.find('input[name="prev-bet-away"]').val();
        var betScore = form.find('input[name="bet_score"]').val();
        var betHtml = (homeScore == '' || awayScore == '') ? '' : '<span class="label label-default bet-label">' + homeScore + ':' + awayScore + '</span>';
		$('.fixed-action-btn').show();
		container.find('.display-bet').remove();
		container.find('.edit-bet').remove();
        container.prepend(displayBetHtml);
        var displayBet = container.find('.display-bet');
        var displayAllBets = container.find('.display-all-bets-icon');
        var editBet = displayBet.find('.edit-bet-icon');
        container.find('.bet').append(betHtml);
        var betLabel = container.find('.bet-label');
        if (startTime > new Date()) {
	        if (!homeScore || !awayScore) {
	        	displayBet.empty();
	        	displayBet.append('<div class="add-bet">Dodaj typ</div>');
	        }
        	displayBet.on("click touchend", function(event) {
            	container.editBet(event);
        	});
	        displayAllBets.hide();
	        editBet.show();
        } else {
        	editBet.hide();
        	displayAllBets.show();
        	if (!homeScore || !awayScore) {
	        	displayBet.empty();
	        	displayBet.append('<div class="add-bet">Brak</div>');
	        } else {
	        	if (betScore == 3) {
	        		betLabel.addClass('white-text');
	        		betLabel.addClass('green');
	        	} else if (betScore == 1) {
	        		betLabel.addClass('white-text');
	        		betLabel.addClass('blue');
	        	}
        	}
        }

    };
    
    $.fn.submitWinnerBet = function() {
    	var form = $('#winner-bet-form');
    	var betId = form.find('input[name="bet[bet_id]"]').val();
    	var url = '/bets';
    	if (betId) {
    		url += '/' + betId;
    		form.find('input[name="_method"]').val("patch");
    	}
    	var betActions = $('#winner-bet .bet-actions');
    	betActions.empty();
    	betActions.append(busyCircleHtml);
    	$.post(url, form.serialize())
    		.done(function(data) {
    			if (data && data.id) {
    				form.find('input[name="bet[bet_id]"]').val(data.id);
    			}
    			var selectedOption = $('#winner-dropdown').find(':selected');
    			var flag = selectedOption.attr('data-icon');
    			var name = selectedOption.text();
    			var winnerDropdown = $('#winner-bet');
    			form.find('input[name="team-id"]').val(selectedOption.val());
    			form.find('input[name="team-name"]').val(name);
    			form.find('input[name="team-flag"]').val(selectedOption.attr('data-icon'));
    			$('#winner-bet').displayWinnerBet();
    		}).error(function() {
    			var container = $('#winner-bet');
    			container.editWinnerBet();
            	container.find('.select-dropdown').css('border-bottom-color','#f44336').css('color', '#f44336');
                container.find('.bet-error').text("Błąd podczas zapisu");
    		});
    };
    
    $.fn.displayWinnerBet = function() {
    	var winnerForm = $('#winner-bet-form');
		var name = winnerForm.find('input[name="team-name"]').val();
		var flag = winnerForm.find('input[name="team-flag"]').val();
		if (name) {
			$(this).empty();
			$(this).append(displayWinnerBetHtml);
			$(this).find('img').attr('src', flag);
			$(this).find('.winner-team-name').text(name);
       	} else {
       		$(this).empty();
			$(this).append('<div class="display-winner-bet"><div class="add-bet">Dodaj typ</div></div>');
        }
        if ($('#winner-bet-form').find('input[name="is_editable"]').val() === "true") {
	        $(this).on("click touchend", function(event) {
	        	$(this).editWinnerBet(event);
	        });
        } else {
        	var editBetIcon = $(this).find('.edit-bet-icon');
        	if (editBetIcon.length == 0) {
    			$(this).empty();
				$(this).append(displayWinnerBetHtml);
				var displayWinnerBet = $(this).find('.display-winner-bet');
				editBetIcon = $(this).find('.edit-bet-icon');
				displayWinnerBet.empty();
				displayWinnerBet.append('<div class="add-bet">Brak</div>');
    		}
    		editBetIcon.empty();
    		editBetIcon.append(displayAllBetsHtml);
    		$(this).unbind('click touchend');
        }
	};
    
    $.fn.editWinnerBet = function(event) {
    	$(this).empty();
        $(this).append(editWinnerHtml);
        $(this).unbind('click touchend');
        var winnerDropdown = $('#winner-dropdown');
        var currentTeamId = $('#winner-bet-form').find('input[name="team-id"]').val();
        winnerDropdown.val(currentTeamId);
        winnerDropdown.material_select();
    	$('#add-winner-button').on("click touchend", function() {
        	$(this).submitWinnerBet();
        });
        if (event) {
        event.stopPropagation();
	        $(this).bind("click touchend", function(event) {
	        	event.stopPropagation(event);
	        });
        }
    };
    
    $.fn.disableWinnerBet = function() {
    	var winnerBetForm = $('#winner-bet-form');
    	winnerBetForm.find('input[name="is_editable"]').val("false");
    	winnerBetForm.find('.winner-deadline-text').remove();
    	winnerBetForm.find('.winner-deadline').remove();
    	$('#winner-bet').displayWinnerBet();
    };
    
(function($){
	$( document ).ready(function() {
		$(".button-collapse").sideNav({
		      edge: 'right'
		});
	});
})(jQuery);