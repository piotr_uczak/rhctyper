class CompetitionsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user!
  before_action :set_competition, only: [:show, :edit, :update, :destroy, :results, :add_user]

  respond_to :html

  def index
    @competitions = Competition.open
    respond_with(@competitions)
  end

  def show
    @my_bets = Bet.for_user_and_competition(current_user, @competition)
    @winner_bet = @competition.winner_bet_for_user(current_user)
    @is_winner_bet_score = @competition.is_winner_bet_scored(current_user)
    @winner_bet_deadline_time = @competition.event.winner_bet_deadline_time
    @winner_bet_deadline_str = @competition.event.winner_bet_deadline_str
    @live_games = @competition.event.games.live
    @completed_games = @competition.event.games.completed
    if @winner_bet.nil?

    else
      @competition.event.all_teams.each do |team|
        if team.id == @winner_bet.advancing_team
          @team = team
        end
      end
    end
    respond_with(@competition)
  end

  def all_winner_bets
    @competition = Competition.find params[:idc]
    @bets = @competition.all_winner_bets
    respond_to do |format|
      format.js 
    end
  end

  def results
    @scoreboard = @competition.get_scoreboard
    respond_with(@scoreboard)
  end

  def join
    @assoc = CompetitionsUsers.create(user: current_user, competition: @competition)
    flash[:notice] = 'Dolaczyles do rozgrywek' if (@assoc.save)
    redirect_to competition_path(@competition)
    #respond_with(@competition)
  end

  def add_user
    @usr = User.find params[:usr]
    @assoc = CompetitionsUsers.create(user: @usr, competition: @competition)
    flash[:notice] = ('Uzytkownik '+@usr.name+' zostal pomyślnie dodany do rozgrywek') if (@assoc.save)
    redirect_to edit_competition_path(@competition)
  end

  def remove_user
    @usr = User.find params[:usr]
    CompetitionsUsers.where(user: @usr).first.destroy
    flash[:notice] = ('Uzytkownik '+@usr.name+' zostal usunięty z rozgrywek') 
    redirect_to edit_competition_path(@competition)
  end                                                                                                                              

  def new
    @competition = Competition.new
    respond_with(@competition)
  end

  def edit
    @candidates = User.where.not(id: @competition.users.ids)
  end

  def create
    @competition = Competition.new
    @competition.creator_id = current_user.id
    @competition.competitions_users.build(user: current_user)
    flash[:notice] = 'Competition was successfully created.' if (@competition.save)
    respond_with(@competition)
  end

  def update
    flash[:notice] = 'Competition was successfully updated.' if @competition.update(competition_params)
    respond_with(@competition)
  end

  def destroy
    @competition.destroy
    respond_with(@competition)
  end

  private
    def set_competition
      @competition = Competition.find(params[:id])
    end

    def competition_params
      params.require(:competition).permit(:event_id, :is_open, :usr)
    end
end
