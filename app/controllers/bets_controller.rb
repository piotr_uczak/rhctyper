class BetsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user!
  before_action :set_bet, only: [:show, :edit, :update, :destroy]

  respond_to :json

  def index
    @bets = Bet.all.where(owner: current_user)
    respond_to do |format|
      format.html
    end
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @bet = Bet.new
    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    begin
      @bet = Bet.new(bet_params)
      @bet.owner_id = current_user.id
      #flash[:notice] = 'Bet was successfully created.' if 
      @bet.save
      respond_with(@bet)
    rescue
      return head(:internal_server_error)
    end
  end

  def update
    begin
      if (@bet.is_editable?)
        #flash[:notice] = 'Bet was successfully updated.' if 
        @bet.update(bet_params)
        respond_with(@bet)
      else 
        return head(:bad_request)
      end
    rescue
      return head(:internal_server_error)
    end
  end

  def destroy
    @bet.destroy
    respond_with(@bet)
  end

  private
    def set_bet
      @bet = Bet.find(params[:id])
    end

    def bet_params
      params.require(:bet).permit(:game_id, :competition_id, :home_score, :away_score, :advancing_team)
    end
end
