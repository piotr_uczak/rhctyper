 class GamesController < ApplicationController
  load_and_authorize_resource
  protect_from_forgery except: :all_bets
  before_action :authenticate_user!
  before_action :set_game, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @games = Game.all
    respond_with(@games)
  end

  def show
    respond_with(@game)
  end

  def new
    @game = Game.new
    respond_with(@game)
  end

  def all_bets
    @bets = @game.all_bets(Competition.find params[:idc])
    respond_to do |format|
      format.js 
    end
  end

  def edit
    
  end

  def create
    @game = Game.new(game_params)
    flash[:notice] = 'Game was successfully created.' if @game.save
    respond_with(@game)
  end

  def update
    flash[:notice] = 'Game was successfully updated.' if @game.update(game_params)  
    redirect_to edit_game_path(@game)
  end

  def destroy
    @game.destroy
    respond_with(@game)
  end

  private
    def set_game
      @game = Game.find(params[:id])
    end

    def game_params
      params.require(:game).permit(:start_date, :finish_date, :home_team_id, :away_team_id, :event_id, :description, :home_score, :away_score)
    end
end
