json.array!(@games) do |game|
  json.extract! game, :id, :start_date, :finish_date, :home_team_id, :away_team_id, :description
  json.url game_url(game, format: :json)
end
