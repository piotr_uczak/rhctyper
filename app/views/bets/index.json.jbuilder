json.array!(@bets) do |bet|
  json.extract! bet, :id, :game_id_id, :competition_id_id, :owner_id_id, :home_score, :away_score, :advancing_team
  json.url bet_url(bet, format: :json)
end
