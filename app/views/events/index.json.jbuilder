json.array!(@events) do |event|
  json.extract! event, :id, :start_date, :name, :logo
  json.url event_url(event, format: :json)
end
