json.array!(@competitions) do |competition|
  json.extract! competition, :id, :event_id_id, :creator_id_id
  json.url competition_url(competition, format: :json)
end
