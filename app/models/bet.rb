class Bet < ActiveRecord::Base
  belongs_to :game
  belongs_to :competition
  belongs_to :owner, class_name: "User"

  #means that for one owner in one competition there can be only one bet of the given game
  # TODO: think if this could not be handled elsewhere
  validates_uniqueness_of :game, scope: [:owner,:competition]

  scope :for_user_and_competition, lambda { |usr,competition|
  	where(owner: usr, competition: competition)
  }
  scope :for_competition, lambda{ |competition|
    where(competition: competition)
  }

  validate :winner_bet_cannot_be_added_after_competition_start

  def winner_bet_cannot_be_added_after_competition_start
    errors.add("Wydarzenie już sie rozpoczelo") if
      game.nil? and competition.event.winner_bet_deadline_time < Time.zone.now
  end

  def is_editable?
  	if (game.nil?) #bet is for the whole competition
  		competition.event.start_date.nil? || competition.event.start_date > Time.zone.now ? true : false
  	else 
  		game.start_date > Time.zone.now ? true : false
  	end
  end

  def validate_result(actual_home_score, actual_away_score)
    if (actual_away_score == self.away_score && actual_home_score == self.home_score)
      # dokladny wynik trafiony
      self.points_earned = 3
    elsif (actual_home_score == actual_away_score && self.home_score == self.away_score) ||
        (actual_home_score < actual_away_score && self.home_score < self.away_score) ||
        (actual_home_score > actual_away_score && self.home_score > self.away_score)
      # trafione rozstrzygniecie
      self.points_earned = 1
    else
      self.points_earned = 0
    end
    self.update_attribute(:points_earned, self.points_earned)
  end
end
