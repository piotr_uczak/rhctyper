class Competition < ActiveRecord::Base
  belongs_to :event
  belongs_to :creator, class_name: "User"
  has_many :bets
  has_many :competitions_users, class_name: "CompetitionsUsers"
  has_many :users, :through => :competitions_users
  
  accepts_nested_attributes_for :competitions_users

  validates :event_id, presence: true

  scope :open, lambda { ||
    where(is_open: true)
  }

  def points_for_user(user)   
    score = 0
  	user.bets.where(:competition => self).each do |bet|
  		score += (bet.points_earned!=nil ? bet.points_earned : 0)
  	end
    return score
  end

  def winner_bet_for_user(user) 
    user.bets.where(:competition => self, :game => nil).take
  end
  
  def is_winner_bet_scored(user)
    user.bets.where(:competition => self, :game => nil, 'points_earned' => 10).take
  end

  def all_winner_bets
    @wBets = []
    if event.winner_bet_deadline_time < Time.zone.now
      users.each do |usr|
        @wBets << winner_bet_for_user(usr)
      end
    end
    @wBets
  end

  def user_belongs_to(user)
    competitions_users.exists?(:user_id => user.id)
  end

  def get_scoreboard
    scores = Hash.new
    users.each do |usr|
      scores[usr] = points_for_user(usr)
    end
    return scores.sort_by { |usr, score| score}.reverse
  end
end
