class Game < ActiveRecord::Base
	belongs_to :event
	belongs_to :home_team, :class_name => "Team"
	belongs_to :away_team, :class_name => "Team"
	has_many :bets

	default_scope { order('start_date ASC') }

	scope :incomming, ->() { where("start_date > ?", Time.zone.now) }
	scope :live, ->() { where("start_date <= ? AND finish_date is NULL", Time.zone.now) }
	scope :completed, ->() { where("start_date < ? AND finish_date is not NULL", Time.zone.now).reorder('finish_date DESC') }

	after_update :update_related_bets

	def is_live 
		start_date <= Time.zone.now && finish_date.nil
	end

	def bet_for_user(user)
		self.bets.where(:owner => user)
	end

	def all_bets(competition)
		if (self.can_user_see_other_bets?)
			bets.for_competition(competition).group_by(&:owner).sort_by { |usr, bet|
			 bet.first.points_earned }.reverse
		end
	end

	def game_day
		if (self.start_date.today?) 
			"Dziś"
		elsif (self.start_date.to_date == Date.tomorrow)
			"Jutro"
		else
			I18n.l(self.start_date, format: :short)
		end
	end

	def day
		self.start_date.strftime('%D')
	end

	def is_bettable
		(self.home_team!=nil && self.away_team!=nil && self.start_date > Time.zone.now) ? true : false
	end

	def can_user_see_other_bets?
		self.start_date <= Time.zone.now ? true : false
	end

	protected 
		def update_related_bets
			logger.info "update requested"
			unless self.home_score == nil || self.away_score == nil
				logger.info "updating related bets"
				self.bets.each do |b|
					b.validate_result(self.home_score, self.away_score)	
				end
			end
		end
end
