class Team < ActiveRecord::Base
  belongs_to :user
  has_many :games, foreign_key: "home_team_id"
  has_many :games, foreign_key: "away_team_id"
  has_attached_file :flag, :styles => { :small => "150x150>", :tiny => "34x34" }, default_url: "/images/:style/missing.png"
  
  #validates_attachment_presence :flag
  validates_attachment_size :flag, :less_than => 2.megabytes
  validates_attachment_content_type :flag, :content_type => ['image/jpeg', 'image/png']
  scope :by_name, -> name { where(name: name)[0] }
end
