class Event < ActiveRecord::Base
	has_many :games
	has_many :competition
	has_many :teams, :through => :games, :source => :home_team
	belongs_to :creator, class_name: "User"
	has_attached_file :logo, :styles => { :small => "150x150>" }

	#validates_attachment_presence :logo
	validates_attachment_size :logo, :less_than => 2.megabytes
	validates_attachment_content_type :logo, :content_type => ['image/jpeg', 'image/png']

	def all_teams
		self.teams.uniq
	end

	
	def winner_bet_deadline_str
	  @bet_deadline = winner_bet_deadline_time
	  if (@bet_deadline.today?)
	    @bet_deadline.strftime("Dziś %H:%M")
	  elsif (@bet_deadline == Date.tomorrow)
	    @bet_deadline.strftime("Jutro %H:%M")
	  else
	    @bet_deadline.strftime("%F %H:%M")
	  end
	end
	
	def winner_bet_deadline_time
	  self.games.order('start_date ASC').first.start_date
	end
end
