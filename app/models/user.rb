class User < ActiveRecord::Base
  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?
  has_many :team
  has_many :bets, :foreign_key => :owner_id
  has_many :competitions_users, :class_name => "CompetitionsUsers"
  has_many :competitions, :through => :competitions_users

  #validates :name, presence: true

  def set_default_role
    self.role ||= :user
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :lockable, :trackable, :validatable
end
