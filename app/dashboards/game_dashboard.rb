require "administrate/base_dashboard"

class GameDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    event: Field::BelongsTo,
    home_team: Field::BelongsTo,
    away_team: Field::BelongsTo,
    bets: Field::HasMany,
    id: Field::Number,
    start_date: Field::DateTime,
    finish_date: Field::DateTime,
    home_ht_score: Field::Number,
    away_ht_score: Field::Number,
    home_score: Field::Number,
    away_score: Field::Number,
    extra_time: Field::Boolean,
    penalties: Field::Boolean,
    home_et_score: Field::Number,
    away_et_score: Field::Number,
    home_penalty_score: Field::Number,
    away_penalty_score: Field::Number,
    location: Field::String,
    city: Field::String,
    game_group: Field::String,
    description: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    event
    home_team
    away_team
    bets
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    event
    home_team
    away_team
    bets
    id
    start_date
    finish_date
    home_ht_score
    away_ht_score
    home_score
    away_score
    extra_time
    penalties
    home_et_score
    away_et_score
    home_penalty_score
    away_penalty_score
    location
    city
    game_group
    description
    created_at
    updated_at
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    event
    home_team
    away_team
    bets
    start_date
    finish_date
    home_ht_score
    away_ht_score
    home_score
    away_score
    extra_time
    penalties
    home_et_score
    away_et_score
    home_penalty_score
    away_penalty_score
    location
    city
    game_group
    description
  ].freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { resources.where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how games are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(game)
  #   "Game ##{game.id}"
  # end
end
