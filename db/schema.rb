# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150712073729) do

  create_table "bets", force: true do |t|
    t.integer  "game_id"
    t.integer  "competition_id"
    t.integer  "owner_id"
    t.integer  "home_score"
    t.integer  "away_score"
    t.integer  "advancing_team"
    t.integer  "points_earned"
    t.boolean  "is_won"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bets", ["competition_id"], name: "index_bets_on_competition_id"
  add_index "bets", ["game_id"], name: "index_bets_on_game_id"
  add_index "bets", ["owner_id"], name: "index_bets_on_owner_id"

  create_table "competitions", force: true do |t|
    t.integer  "event_id"
    t.integer  "creator_id"
    t.boolean  "is_open"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "competitions", ["creator_id"], name: "index_competitions_on_creator_id"
  add_index "competitions", ["event_id"], name: "index_competitions_on_event_id"

  create_table "competitions_users", force: true do |t|
    t.integer  "competition_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "competitions_users", ["competition_id"], name: "index_competitions_users_on_competition_id"
  add_index "competitions_users", ["user_id"], name: "index_competitions_users_on_user_id"

  create_table "events", force: true do |t|
    t.datetime "start_date"
    t.integer  "creator_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  add_index "events", ["creator_id"], name: "index_events_on_creator_id"

  create_table "games", force: true do |t|
    t.datetime "start_date"
    t.datetime "finish_date"
    t.integer  "home_team_id"
    t.integer  "away_team_id"
    t.integer  "event_id"
    t.integer  "home_ht_score"
    t.integer  "away_ht_score"
    t.integer  "home_score"
    t.integer  "away_score"
    t.boolean  "extra_time"
    t.boolean  "penalties"
    t.integer  "home_et_score"
    t.integer  "away_et_score"
    t.integer  "home_penalty_score"
    t.integer  "away_penalty_score"
    t.string   "location"
    t.string   "city"
    t.string   "game_group"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teams", force: true do |t|
    t.string   "name"
    t.boolean  "national"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "flag_file_name"
    t.string   "flag_content_type"
    t.integer  "flag_file_size"
    t.datetime "flag_updated_at"
  end

  add_index "teams", ["user_id"], name: "index_teams_on_user_id"

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "role"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      default: 0
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count"
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id"
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
