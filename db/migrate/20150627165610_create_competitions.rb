class CreateCompetitions < ActiveRecord::Migration[4.2]
  def change
    create_table :competitions do |t|
      t.references :event, index: true
      t.references :creator, index: true
      t.boolean :is_open
      
      t.timestamps
    end
  end
end
