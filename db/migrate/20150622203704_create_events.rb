class CreateEvents < ActiveRecord::Migration[4.2]
  def change
    create_table :events do |t|
      t.datetime :start_date
      t.references :creator, index: true
      t.string :name

      t.timestamps
    end
  end
end
