class CreateGames < ActiveRecord::Migration[4.2]
  def change
    create_table :games do |t|
      t.datetime :start_date
      t.datetime :finish_date
      t.integer :home_team_id
      t.integer :away_team_id
      t.integer :event_id
      t.integer :home_ht_score
      t.integer :away_ht_score
      t.integer :home_score
      t.integer :away_score
      t.boolean :extra_time
      t.boolean :penalties
      t.integer :home_et_score
      t.integer :away_et_score
      t.integer :home_penalty_score
      t.integer :away_penalty_score
      t.string :location
      t.string :city
      t.string :game_group
      t.text :description 

      t.timestamps
    end
  end
end
