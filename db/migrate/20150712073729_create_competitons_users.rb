class CreateCompetitionsUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :competitions_users do |t|
      t.references :competition, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
