class CreateBets < ActiveRecord::Migration[4.2]
  def change
    create_table :bets do |t|
      t.references :game, index: true
      t.references :competition, index: true
      t.references :owner, index: true
      t.integer :home_score
      t.integer :away_score
      t.integer :advancing_team
      t.integer :points_earned
      t.boolean :is_won

      t.timestamps
    end
  end
end
