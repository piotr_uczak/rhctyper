class CreateTeams < ActiveRecord::Migration[4.2]
  def change
    create_table :teams do |t|
      t.string :name
      t.boolean :national
      t.references :user, index: true

      t.timestamps
    end
  end
end
