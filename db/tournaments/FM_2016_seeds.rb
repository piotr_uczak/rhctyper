class Friendly2016Seeds
	@@event

	def self.SeedEvent
		@@event = Event.create!(name: "Mecze towarzyskie 2016")
	end

	def self.event
		@@event
	end

	def self.SeedTeams
		Team.create!(name: "Holandia", national:true, flag_file_name: "nl.png", flag_content_type: "image/png")
	end

	def self.SeedMatches
Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Holandia"), 
			start_date: Time.new(2016, 6, 1,  20,  45,  0), event_id: @@event.id, game_group: "Mecz towarzyski", location: "Stadion ENERGA", city: "Gdansk")
	end
end