class EURO2020Seeds
	@@event

	def self.SeedEvent
		@@event = Event.create!(name: "Euro 2020")
	end

	def self.event
		@@event
	end

	def self.SeedTeams
		Team.create!(name: "Polska", national:true, flag_file_name: "300px-flaga-polski.png",  flag_content_type: "image/png", flag_file_size: 537, flag_updated_at: "2015-07-11 19:51:42")
		Team.create!(name: "Niemcy", national:true, flag_file_name: "Flaga_Niemiec.png", flag_content_type: "image/png", flag_file_size: 1929, flag_updated_at: "2015-07-11 19:53:49")
		Team.create!(name: "Anglia", national:true, flag_file_name: "Flaga_Anglii.png", flag_content_type: "image/png", flag_file_size: 1514, flag_updated_at: "2015-07-11 19:54:42")
		Team.create!(name: "Ukraina", national:true, flag_file_name: "ua.png", flag_content_type: "image/png")
		Team.create!(name: "Szwajcaria", national:true, flag_file_name: "ch.png", flag_content_type: "image/png")
		Team.create!(name: "Chorwacja", national:true, flag_file_name: "hr.png", flag_content_type: "image/png")
		Team.create!(name: "Hiszpania", national:true, flag_file_name: "es.png", flag_content_type: "image/png")
		Team.create!(name: "Francja", national:true, flag_file_name: "fr.png", flag_content_type: "image/png")
		Team.create!(name: "Belgia", national:true, flag_file_name: "be.png", flag_content_type: "image/png")
		Team.create!(name: "Włochy", national:true, flag_file_name: "it.png", flag_content_type: "image/png")
		Team.create!(name: "Czechy", national:true, flag_file_name: "cz.png", flag_content_type: "image/png")
		Team.create!(name: "Portugalia", national:true, flag_file_name: "pt.png", flag_content_type: "image/png")
		Team.create!(name: "Walia", national:true, flag_file_name: "wl.png", flag_content_type: "image/png")
		Team.create!(name: "Szwecja", national:true, flag_file_name: "se.png", flag_content_type: "image/png")
		Team.create!(name: "Austria", national:true, flag_file_name: "at.png", flag_content_type: "image/png")
		Team.create!(name: "Turcja", national:true, flag_file_name: "tr.png", flag_content_type: "image/png")
		Team.create!(name: "Rosja", national:true, flag_file_name: "ru.png", flag_content_type: "image/png")
		Team.create!(name: "Węgry", national:true, flag_file_name: "hu.png", flag_content_type: "image/png")
		Team.create!(name: "Słowacja", national:true, flag_file_name: "sk.png", flag_content_type: "image/png")
		Team.create!(name: "Macedonia Północna", national:true, flag_file_name: "mcd.png", flag_content_type: "image/png")
		Team.create!(name: "Dania", national:true, flag_file_name: "dk.png", flag_content_type: "image/png")
		Team.create!(name: "Holandia", national:true, flag_file_name: "nl.png", flag_content_type: "image/png")
		Team.create!(name: "Finlandia", national:true, flag_file_name: "fi.png", flag_content_type: "image/png")
		Team.create!(name: "Szkocja", national:true, flag_file_name: "sc.png", flag_content_type: "image/png")
    #Team.create!(name: "-",national:true)
	end

	def self.SeedMatches
#1 runda
	#11.06
		Game.create!(home_team: Team.by_name("Turcja"), away_team: Team.by_name("Włochy"),
			start_date: Time.new(2021, 6, 11,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadio Olimpico", city: "Rzym")
	#12.06
		Game.create!(home_team: Team.by_name("Walia"), away_team: Team.by_name("Szwajcaria"),
			start_date: Time.new(2021, 6, 12,  15,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion Olimpijski", city: "Baku")
		Game.create!(home_team: Team.by_name("Dania"), away_team: Team.by_name("Finlandia"),
			start_date: Time.new(2021, 6, 12,  18,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Parken", city: "Kopenhaga")
		Game.create!(home_team: Team.by_name("Belgia"), away_team: Team.by_name("Rosja"),
			start_date: Time.new(2021, 6, 12,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion Kriestowskij", city: "Petersburg")
	#13.06
    Game.create!(home_team: Team.by_name("Anglia"), away_team: Team.by_name("Chorwacja"),
  		start_date: Time.new(2021, 6, 13,  15,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadion Wembley", city: "Londyn")
		Game.create!(home_team: Team.by_name("Austria"), away_team: Team.by_name("Macedonia Północna"),
			start_date: Time.new(2021, 6, 13,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Arena Narodowa", city: "Bukareszt")
		Game.create!(home_team: Team.by_name("Holandia"), away_team: Team.by_name("Ukraina"),
			start_date: Time.new(2021, 6, 13,  21,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Johan Cruyff ArenA", city: "Amsterdam")
	#14.06
		Game.create!(home_team: Team.by_name("Szkocja"), away_team: Team.by_name("Czechy"),
			start_date: Time.new(2021, 6, 14,  15,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Hampden Park", city: "Glasgow")
		Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Słowacja"),
			start_date: Time.new(2021, 6, 14,  18,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stadion Kriestowskij", city: "Petersburg")
		Game.create!(home_team: Team.by_name("Hiszpania"), away_team: Team.by_name("Szwecja"),
			start_date: Time.new(2021, 6, 14,  21,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Estadio La Cartuja", city: "Sewilla")
	#15.06
		Game.create!(home_team: Team.by_name("Węgry"), away_team: Team.by_name("Portugalia"),
			start_date: Time.new(2021, 6, 15,  18,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Puskás Aréna", city: "Budapeszt")
		Game.create!(home_team: Team.by_name("Francja"), away_team: Team.by_name("Niemcy"),
			start_date: Time.new(2021, 6, 15,  21,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Allianz Arena", city: "Monachium")
#2 runda
	#16.06
		Game.create!(home_team: Team.by_name("Finlandia"), away_team: Team.by_name("Rosja"),
			start_date: Time.new(2021, 6, 16,  15,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Parken", city: "Kopenhaga")
		Game.create!(home_team: Team.by_name("Turcja"), away_team: Team.by_name("Walia"),
			start_date: Time.new(2021, 6, 16,  18,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion Olimpijski", city: "Baku")
		Game.create!(home_team: Team.by_name("Włochy"), away_team: Team.by_name("Szwajcaria"),
			start_date: Time.new(2021, 6, 16,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadio Olimpico", city: "Rzym")
	#17.06
		Game.create!(home_team: Team.by_name("Ukraina"), away_team: Team.by_name("Macedonia Północna"),
			start_date: Time.new(2021, 6, 17,  15,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Arena Narodowa", city: "Bukareszt")
		Game.create!(home_team: Team.by_name("Dania"), away_team: Team.by_name("Belgia"),
			start_date: Time.new(2021, 6, 17,  18,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Parken", city: "Kopenhaga")
		Game.create!(home_team: Team.by_name("Holandia"), away_team: Team.by_name("Austria"),
			start_date: Time.new(2021, 6, 17,  21,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Johan Cruyff ArenA", city: "Amsterdam")
	#18.06
		Game.create!(home_team: Team.by_name("Szwecja"), away_team: Team.by_name("Słowacja"),
			start_date: Time.new(2021, 6, 18,  15,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stadion Kriestowskij", city: "Petersburg")
		Game.create!(home_team: Team.by_name("Chorwacja"), away_team: Team.by_name("Czechy"),
			start_date: Time.new(2021, 6, 18,  18,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Hampden Park", city: "Glasgow")
		Game.create!(home_team: Team.by_name("Anglia"), away_team: Team.by_name("Szkocja"),
			start_date: Time.new(2021, 6, 18,  21,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadion Wembley", city: "Londyn")
	#19.06
		Game.create!(home_team: Team.by_name("Węgry"), away_team: Team.by_name("Francja"),
			start_date: Time.new(2021, 6, 19,  15,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Puskás Aréna", city: "Budapeszt")
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Niemcy"),
			start_date: Time.new(2021, 6, 19,  18,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Allianz Arena", city: "Monachium")
		Game.create!(home_team: Team.by_name("Hiszpania"), away_team: Team.by_name("Polska"),
			start_date: Time.new(2021, 6, 19,  21,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Estadio La Cartuja", city: "Sewilla")
#3 runda
	#20.06
		Game.create!(home_team: Team.by_name("Szwajcaria"), away_team: Team.by_name("Turcja"),
			start_date: Time.new(2021, 6, 20,  18,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion Olimpijski", city: "Baku")
		Game.create!(home_team: Team.by_name("Włochy"), away_team: Team.by_name("Walia"),
			start_date: Time.new(2021, 6, 20,  18,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadio Olimpico", city: "Rzym")
	#21.06
		Game.create!(home_team: Team.by_name("Macedonia Północna"), away_team: Team.by_name("Holandia"),
			start_date: Time.new(2021, 6, 21,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Johan Cruyff ArenA", city: "Amsterdam")
		Game.create!(home_team: Team.by_name("Ukraina"), away_team: Team.by_name("Austria"),
			start_date: Time.new(2021, 6, 21,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion Narodowy", city: "Bukareszt")
    Game.create!(home_team: Team.by_name("Rosja"), away_team: Team.by_name("Dania"),
      start_date: Time.new(2021, 6, 21,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Parken", city: "Kopenhaga")
    Game.create!(home_team: Team.by_name("Finlandia"), away_team: Team.by_name("Belgia"),
      start_date: Time.new(2021, 6, 21,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion Kriestowskij", city: "Petersburg")
	#22.06
		Game.create!(home_team: Team.by_name("Chorwacja"), away_team: Team.by_name("Szkocja"),
			start_date: Time.new(2021, 6, 22,  21,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Hampden Park", city: "Glasgow")
		Game.create!(home_team: Team.by_name("Czechy"), away_team: Team.by_name("Anglia"),
			start_date: Time.new(2021, 6, 22,  21,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadion Wembley", city: "Londyn")
	#23.06
		Game.create!(home_team: Team.by_name("Słowacja"), away_team: Team.by_name("Hiszpania"),
			start_date: Time.new(2021, 6, 23,  18,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Estadio La Cartuja", city: "Sewilla")
		Game.create!(home_team: Team.by_name("Szwecja"), away_team: Team.by_name("Polska"),
			start_date: Time.new(2021, 6, 23,  18,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stadion Kriestowskij", city: "Petersburg")
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Francja"),
			start_date: Time.new(2021, 6, 23,  21,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Puskás Aréna", city: "Budapeszt")
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Węgry"),
			start_date: Time.new(2021, 6, 23,  21,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Allianz Arena", city: "Monachium")
#1/8 finału
	#26.06
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 6, 26,  18,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Johan Cruyff ArenA", city: "Amsterdam")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 6, 26,  21,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Stadion Wembley", city: "Londyn")
	#27.06
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 6, 27,  18,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Puskás Aréna", city: "Budapeszt")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 6, 27,  21,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Estadio La Cartuja", city: "Sewilla")
	#28.06
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 6, 28,  18,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Parken", city: "Kopenhaga")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 6, 28,  21,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Arena Narodowa", city: "Bukareszt")
	#29.06
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 6, 29,  18,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Stadion Wembley", city: "Londyn")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 6, 29,  21,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Hampden Park", city: "Glasgow")

#Ćwierćfinały
	#02.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 7, 2,  18,  00,  0), event_id: @@event.id, game_group: "Ćwierćfinał", location: "Stadion Kriestowskij", city: "Petersburg")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 7, 2,  21,  00,  0), event_id: @@event.id, game_group: "Ćwierćfinał", location: "Allianz Arena", city: "Monachium")

	#03.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 7, 3,  18,  00,  0), event_id: @@event.id, game_group: "Ćwierćfinał", location: "Stadion Olimpijski", city: "Baku")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 7, 3,  21,  00,  0), event_id: @@event.id, game_group: "Ćwierćfinał", location: "Stadio Olimpico", city: "Rzym")

#półfinały
	#06.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 7, 6,  21,  00,  0), event_id: @@event.id, game_group: "Półfinał", location: "Stadion Wembley", city: "Londyn")

	#07.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 7, 7,  21,  00,  0), event_id: @@event.id, game_group: "Półfinał", location: "Stadion Wembley", city: "Londyn")

#finał
	#11.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2021, 7, 11,  21,  00,  0), event_id: @@event.id, game_group: "FINAŁ", location: "Stadion Wembley", city: "Londyn")

	end
end