class EURO2024Seeds
	@@event

	def self.SeedEvent
		@@event = Event.create!(name: "Euro 2024")
	end

	def self.event
		@@event
	end

	def self.SeedMatches
#1 runda
	#14.06
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Szkocja"),
			start_date: Time.new(2024, 6, 14,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Allianz Arena", city: "Monachium")
	#15.06
		Game.create!(home_team: Team.by_name("Węgry"), away_team: Team.by_name("Szwajcaria"),
			start_date: Time.new(2024, 6, 15,  15,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "RheinEnergieStadion", city: "Kolonia")
		Game.create!(home_team: Team.by_name("Hiszpania"), away_team: Team.by_name("Chorwacja"),
			start_date: Time.new(2024, 6, 15,  18,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion Olimpijski", city: "Berlin")
		Game.create!(home_team: Team.by_name("Włochy"), away_team: Team.by_name("Albania"),
			start_date: Time.new(2024, 6, 15,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Signal Iduna Park", city: "Dortmund")
	#16.06
		Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Holandia"),
			start_date: Time.new(2024, 6, 16,  15,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Volksparkstadion", city: "Hamburg")
		Game.create!(home_team: Team.by_name("Słowenia"), away_team: Team.by_name("Dania"),
			start_date: Time.new(2024, 6, 16,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Mercedes-Benz Arena", city: "Stuttgart")
		Game.create!(home_team: Team.by_name("Serbia"), away_team: Team.by_name("Anglia"),
			start_date: Time.new(2024, 6, 16,  21,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Veltins-Arena", city: "Gelsenkirchen")
	#17.06
		Game.create!(home_team: Team.by_name("Rumunia"), away_team: Team.by_name("Ukraina"),
			start_date: Time.new(2024, 6, 17,  15,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Allianz Arena", city: "Monachium")
		Game.create!(home_team: Team.by_name("Belgia"), away_team: Team.by_name("Słowacja"),
			start_date: Time.new(2024, 6, 17,  18,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Deutsche Bank Park", city: "Frankfurt")
		Game.create!(home_team: Team.by_name("Austria"), away_team: Team.by_name("Francja"),
			start_date: Time.new(2024, 6, 17,  21,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Esprit Arena", city: "Düsseldorf")
	#18.06
		Game.create!(home_team: Team.by_name("Turcja"), away_team: Team.by_name("Gruzja"),
			start_date: Time.new(2024, 6, 18,  18,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Signal Iduna Park", city: "Dortmund")
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Czechy"),
			start_date: Time.new(2024, 6, 18,  21,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Red Bull Arena", city: "Lipsk")
#2 runda
	#19.06
		Game.create!(home_team: Team.by_name("Chorwacja"), away_team: Team.by_name("Albania"),
			start_date: Time.new(2024, 6, 19,  15,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Volksparkstadion", city: "Hamburg")
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Węgry"),
			start_date: Time.new(2024, 6, 19,  18,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Mercedes-Benz Arena", city: "Stuttgart")
		Game.create!(home_team: Team.by_name("Szkocja"), away_team: Team.by_name("Szwajcaria"),
			start_date: Time.new(2024, 6, 19,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "RheinEnergieStadion", city: "Kolonia")
	#20.06
		Game.create!(home_team: Team.by_name("Słowenia"), away_team: Team.by_name("Serbia"),
			start_date: Time.new(2024, 6, 20,  15,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Allianz Arena", city: "Monachium")
		Game.create!(home_team: Team.by_name("Dania"), away_team: Team.by_name("Anglia"),
			start_date: Time.new(2024, 6, 20,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Deutsche Bank Park", city: "Frankfurt")
		Game.create!(home_team: Team.by_name("Hiszpania"), away_team: Team.by_name("Włochy"),
			start_date: Time.new(2024, 6, 20,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Veltins-Arena", city: "Gelsenkirchen")
	#21.06
		Game.create!(home_team: Team.by_name("Słowacja"), away_team: Team.by_name("Ukraina"),
			start_date: Time.new(2024, 6, 21,  15,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Esprit Arena", city: "Düsseldorf")
		Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Austria"),
			start_date: Time.new(2024, 6, 21,  18,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadion Olimpijski", city: "Berlin")
		Game.create!(home_team: Team.by_name("Holandia"), away_team: Team.by_name("Francja"),
			start_date: Time.new(2024, 6, 21,  21,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Red Bull Arena", city: "Lipsk")
	#22.06
		Game.create!(home_team: Team.by_name("Gruzja"), away_team: Team.by_name("Czechy"),
			start_date: Time.new(2024, 6, 22,  15,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Volksparkstadion", city: "Hamburg")
		Game.create!(home_team: Team.by_name("Turcja"), away_team: Team.by_name("Portugalia"),
			start_date: Time.new(2024, 6, 22,  18,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Signal Iduna Park", city: "Dortmund")
		Game.create!(home_team: Team.by_name("Belgia"), away_team: Team.by_name("Rumunia"),
			start_date: Time.new(2024, 6, 22,  21,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "RheinEnergieStadion", city: "Kolonia")
#3 runda
	#23.06
		Game.create!(home_team: Team.by_name("Szwajcaria"), away_team: Team.by_name("Niemcy"),
			start_date: Time.new(2024, 6, 23,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Deutsche Bank Park", city: "Frankfurt")
		Game.create!(home_team: Team.by_name("Szkocja"), away_team: Team.by_name("Węgry"),
			start_date: Time.new(2024, 6, 23,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Mercedes-Benz Arena", city: "Stuttgart")
	#24.06
		Game.create!(home_team: Team.by_name("Albania"), away_team: Team.by_name("Hiszpania"),
			start_date: Time.new(2024, 6, 24,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Esprit Arena", city: "Düsseldorf")
		Game.create!(home_team: Team.by_name("Chorwacja"), away_team: Team.by_name("Włochy"),
			start_date: Time.new(2024, 6, 24,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Red Bull Arena", city: "Lipsk")
 	#25.06
	 	Game.create!(home_team: Team.by_name("Francja"), away_team: Team.by_name("Polska"),
	 		start_date: Time.new(2024, 6, 25,  18,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Signal Iduna Park", city: "Dortmund")
 		Game.create!(home_team: Team.by_name("Holandia"), away_team: Team.by_name("Austria"),
	 		start_date: Time.new(2024, 6, 25,  18,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadion Olimpijski", city: "Berlin")
		Game.create!(home_team: Team.by_name("Dania"), away_team: Team.by_name("Serbia"),
      		start_date: Time.new(2024, 6, 25,  21,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Allianz Arena", city: "Monachium")
    	Game.create!(home_team: Team.by_name("Anglia"), away_team: Team.by_name("Słowenia"),
      		start_date: Time.new(2024, 6, 25,  21,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "RheinEnergieStadion", city: "Kolonia")
	#26.06
		Game.create!(home_team: Team.by_name("Słowacja"), away_team: Team.by_name("Rumunia"),
			start_date: Time.new(2024, 6, 26,  18,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Deutsche Bank Park", city: "Frankfurt")
		Game.create!(home_team: Team.by_name("Ukraina"), away_team: Team.by_name("Belgia"),
			start_date: Time.new(2024, 6, 26,  18,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Mercedes-Benz Arena", city: "Stuttgart")
		Game.create!(home_team: Team.by_name("Gruzja"), away_team: Team.by_name("Portugalia"),
			start_date: Time.new(2024, 6, 26,  21,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Veltins-Arena", city: "Gelsenkirchen")
		Game.create!(home_team: Team.by_name("Czechy"), away_team: Team.by_name("Turcja"),
			start_date: Time.new(2024, 6, 26,  21,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Volksparkstadion", city: "Hamburg")
#1/8 finału
	#29.06
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 6, 29,  18,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Stadion Olimpijski", city: "Berlin")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 6, 29,  21,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Signal Iduna Park", city: "Dortmund")
	#30.06
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 6, 30,  18,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Veltins-Arena", city: "Gelsenkirchen")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 6, 30,  21,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "RheinEnergieStadion", city: "Kolonia")
	#01.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 1,  18,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Esprit Arena", city: "Düsseldorf")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 1,  21,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Deutsche Bank Park", city: "Frankfurt")
	#02.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 2,  18,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Allianz Arena", city: "Monachium")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 2,  21,  00,  0), event_id: @@event.id, game_group: "1/8 finału", location: "Red Bull Arena", city: "Lipsk")

#Ćwierćfinały
	#05.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 5,  18,  00,  0), event_id: @@event.id, game_group: "Ćwierćfinał", location: "Mercedes-Benz Arena", city: "Stuttgart")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 5,  21,  00,  0), event_id: @@event.id, game_group: "Ćwierćfinał", location: "Volksparkstadion", city: "Hamburg")

	#06.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 6,  18,  00,  0), event_id: @@event.id, game_group: "Ćwierćfinał", location: "Esprit Arena", city: "Düsseldorf")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 6,  21,  00,  0), event_id: @@event.id, game_group: "Ćwierćfinał", location: "Stadion Olimpijski", city: "Berlin")

#półfinały
	#09.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 9,  21,  00,  0), event_id: @@event.id, game_group: "Półfinał", location: "Allianz Arena", city: "Monachium")

	#10.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 10,  21,  00,  0), event_id: @@event.id, game_group: "Półfinał", location: "Signal Iduna Park", city: "Dortmund")

#finał
	#14.07
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2024, 7, 14,  21,  00,  0), event_id: @@event.id, game_group: "FINAŁ", location: "Stadion Olimpijski", city: "Berlin")

	end
end