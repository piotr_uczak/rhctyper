class EURO2017Seeds
	@@event

	def self.SeedEvent
		@@event = Event.create!(name: "Euro 2017 U21")
	end

	def self.event
		@@event
	end

	def self.SeedTeams
		#Team.create!(name: "Polska", national:true, flag_file_name: "300px-flaga-polski.png",  flag_content_type: "image/png", flag_file_size: 537, flag_updated_at: "2015-07-11 19:51:42")
		#Team.create!(name: "Niemcy", national:true, flag_file_name: "Flaga_Niemiec.png", flag_content_type: "image/png", flag_file_size: 1929, flag_updated_at: "2015-07-11 19:53:49")
		#Team.create!(name: "Anglia", national:true, flag_file_name: "Flaga_Anglii.png", flag_content_type: "image/png", flag_file_size: 1514, flag_updated_at: "2015-07-11 19:54:42")
		#Team.create!(name: "Czechy", national:true, flag_file_name: "cz.png", flag_content_type: "image/png")
		#Team.create!(name: "Portugalia", national:true, flag_file_name: "pt.png", flag_content_type: "image/png")
		#Team.create!(name: "Słowacja", national:true, flag_file_name: "sk.png", flag_content_type: "image/png")
		#Team.create!(name: "Hiszpania", national:true, flag_file_name: "es.png", flag_content_type: "image/png")
		#Team.create!(name: "Szwecja", national:true, flag_file_name: "se.png", flag_content_type: "image/png")
		#Team.create!(name: "Włochy", national:true, flag_file_name: "it.png", flag_content_type: "image/png")
		Team.create!(name: "Macedonia", national:true, flag_file_name: "mcd.png", flag_content_type: "image/png")
		Team.create!(name: "Dania", national:true, flag_file_name: "dk.png", flag_content_type: "image/png")
		Team.create!(name: "Serbia", national:true, flag_file_name: "srb.png", flag_content_type: "image/png")
	end

	def self.SeedMatches
		#Gr. A
		Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Słowacja"), 
			start_date: Time.new(2017, 6, 16,  20,  45,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion", city: "Lublin")
		Game.create!(home_team: Team.by_name("Szwecja"), away_team: Team.by_name("Anglia"), 
			start_date: Time.new(2017, 6, 16,  18,  0,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion", city: "Kielce")
		
		Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Szwecja"), 
			start_date: Time.new(2017, 6, 19,  20,  45,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion", city: "Lublin")
		Game.create!(home_team: Team.by_name("Słowacja"), away_team: Team.by_name("Anglia"), 
			start_date: Time.new(2017, 6, 19,  18,  0,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion", city: "Kielce")
		
		Game.create!(home_team: Team.by_name("Słowacja"), away_team: Team.by_name("Szwecja"), 
			start_date: Time.new(2017, 6, 22,  20,  45,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion", city: "Lublin")
		Game.create!(home_team: Team.by_name("Anglia"), away_team: Team.by_name("Polska"), 
			start_date: Time.new(2017, 6, 22,  20,  45,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion", city: "Kielce")

		#Gr. B
		Game.create!(home_team: Team.by_name("Hiszpania"), away_team: Team.by_name("Macedonia"), 
			start_date: Time.new(2017, 6, 17,  20,  45,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion", city: "Gdynia")
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Serbia"), 
			start_date: Time.new(2017, 6, 17,  18,  0,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion", city: "Bydgoszcz")
		
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Hiszpania"), 
			start_date: Time.new(2017, 6, 20,  20,  45,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion", city: "Gdynia")
		Game.create!(home_team: Team.by_name("Serbia"), away_team: Team.by_name("Macedonia"), 
			start_date: Time.new(2017, 6, 20,  18,  0,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion", city: "Bydgoszcz")
		
		Game.create!(home_team: Team.by_name("Macedonia"), away_team: Team.by_name("Portugalia"), 
			start_date: Time.new(2017, 6, 23,  20,  45,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion", city: "Gdynia")
		Game.create!(home_team: Team.by_name("Serbia"), away_team: Team.by_name("Hiszpania"), 
			start_date: Time.new(2017, 6, 23,  20,  45,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion", city: "Bydgoszcz")

		#Gr. C
		Game.create!(home_team: Team.by_name("Dania"), away_team: Team.by_name("Włochy"), 
			start_date: Time.new(2017, 6, 18,  20,  45,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion", city: "Kraków")
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Czechy"), 
			start_date: Time.new(2017, 6, 18,  18,  0,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion", city: "Tychy")
		
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Dania"), 
			start_date: Time.new(2017, 6, 21,  20,  45,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion", city: "Kraków")
		Game.create!(home_team: Team.by_name("Czechy"), away_team: Team.by_name("Włochy"), 
			start_date: Time.new(2017, 6, 21,  18,  0,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion", city: "Tychy")
		
		Game.create!(home_team: Team.by_name("Włochy"), away_team: Team.by_name("Niemcy"), 
			start_date: Time.new(2017, 6, 24,  20,  45,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion", city: "Kraków")
		Game.create!(home_team: Team.by_name("Czechy"), away_team: Team.by_name("Dania"), 
			start_date: Time.new(2017, 6, 24,  20,  45,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion", city: "Tychy")

		#semis
			Game.create!(home_team: nil, away_team: nil, 
		start_date: Time.new(2017, 6, 27,  21,  00,  0), event_id: @@event.id, game_group: "I półfinał", location: "Stadion", city: "Kraków")

		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2017, 6, 27,  18,  00,  0), event_id: @@event.id, game_group: "II półfinał", location: "Stadion", city: "Tychy")

		#final
			Game.create!(home_team: nil, away_team: nil, 
		start_date: Time.new(2017, 6, 30,  20,  45,  0), event_id: @@event.id, game_group: "FINAŁ", location: "Stadion", city: "Kraków")
	end
end