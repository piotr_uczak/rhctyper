class WC2018Seeds
	@@event

	def self.SeedEvent
		@@event = Event.create!(name: "World Cup 2018")
	end

	def self.event
		@@event
	end

	def self.SeedTeams
		Team.create!(name: "Polska", national:true, flag_file_name: "300px-flaga-polski.png",  flag_content_type: "image/png", flag_file_size: 537, flag_updated_at: "2015-07-11 19:51:42")
		Team.create!(name: "Niemcy", national:true, flag_file_name: "Flaga_Niemiec.png", flag_content_type: "image/png", flag_file_size: 1929, flag_updated_at: "2015-07-11 19:53:49")
		Team.create!(name: "Anglia", national:true, flag_file_name: "Flaga_Anglii.png", flag_content_type: "image/png", flag_file_size: 1514, flag_updated_at: "2015-07-11 19:54:42")
		#Team.create!(name: "Czechy", national:true, flag_file_name: "cz.png", flag_content_type: "image/png")
		Team.create!(name: "Portugalia", national:true, flag_file_name: "pt.png", flag_content_type: "image/png")
		Team.create!(name: "Słowacja", national:true, flag_file_name: "sk.png", flag_content_type: "image/png")
		Team.create!(name: "Hiszpania", national:true, flag_file_name: "es.png", flag_content_type: "image/png")
		Team.create!(name: "Szwecja", national:true, flag_file_name: "se.png", flag_content_type: "image/png")
		#Team.create!(name: "Włochy", national:true, flag_file_name: "it.png", flag_content_type: "image/png")
		#Team.create!(name: "Macedonia", national:true, flag_file_name: "mcd.png", flag_content_type: "image/png")
		#Team.create!(name: "Dania", national:true, flag_file_name: "dk.png", flag_content_type: "image/png")
		#Team.create!(name: "Serbia", national:true, flag_file_name: "srb.png", flag_content_type: "image/png")
		Team.create!(name: "Rosja", national:true, flag_file_name: "rus.png", flag_content_type: "image/png")
		Team.create!(name: "Arabia Saudyjska", national:true, flag_file_name: "sau.png", flag_content_type: "image/png")
		Team.create!(name: "Egipt", national:true, flag_file_name: "egy.png", flag_content_type: "image/png")
		Team.create!(name: "Maroko", national:true, flag_file_name: "mar.png", flag_content_type: "image/png")
		Team.create!(name: "Urugwaj", national:true, flag_file_name: "uru.png", flag_content_type: "image/png")
		Team.create!(name: "Iran", national:true, flag_file_name: "ira.png", flag_content_type: "image/png")
		Team.create!(name: "Francja", national:true, flag_file_name: "fra.png", flag_content_type: "image/png")
		Team.create!(name: "Australia", national:true, flag_file_name: "aus.png", flag_content_type: "image/png")
		Team.create!(name: "Argentyna", national:true, flag_file_name: "arg.png", flag_content_type: "image/png")
		Team.create!(name: "Brazylia", national:true, flag_file_name: "bra.png", flag_content_type: "image/png")
		Team.create!(name: "Islandia", national:true, flag_file_name: "ice.png", flag_content_type: "image/png")
		Team.create!(name: "Peru", national:true, flag_file_name: "per.png", flag_content_type: "image/png")
		Team.create!(name: "Dania", national:true, flag_file_name: "dnk.png", flag_content_type: "image/png")
		Team.create!(name: "Chorwacja", national:true, flag_file_name: "cro.png", flag_content_type: "image/png")
		Team.create!(name: "Nigeria", national:true, flag_file_name: "nig.png", flag_content_type: "image/png")
		Team.create!(name: "Senegal", national:true, flag_file_name: "sen.png", flag_content_type: "image/png")
		Team.create!(name: "Japonia", national:true, flag_file_name: "jap.png", flag_content_type: "image/png")
		Team.create!(name: "Kolumbia", national:true, flag_file_name: "col.png", flag_content_type: "image/png")
		Team.create!(name: "Kostaryka", national:true, flag_file_name: "crc.png", flag_content_type: "image/png")
		Team.create!(name: "Korea Południowa", national:true, flag_file_name: "skr.png", flag_content_type: "image/png")
		Team.create!(name: "Belgia", national:true, flag_file_name: "bel.png", flag_content_type: "image/png")
		Team.create!(name: "Panama", national:true, flag_file_name: "pan.png", flag_content_type: "image/png")
		Team.create!(name: "Tunezja", national:true, flag_file_name: "tun.png", flag_content_type: "image/png")
		Team.create!(name: "Meksyk", national:true, flag_file_name: "mex.png", flag_content_type: "image/png")
		Team.create!(name: "Szwajcaria", national:true, flag_file_name: "sui.png", flag_content_type: "image/png")
	end

	def self.DeleteAllMatches
		@@event.games.destroy_all
	end

	def self.SeedMatches
		#Gr. A
		Game.create!(home_team: Team.by_name("Rosja"), away_team: Team.by_name("Arabia Saudyjska"), 
			start_date: Time.new(2018, 6, 14,  17,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion Łużniki", city: "Moskwa")
		Game.create!(home_team: Team.by_name("Egipt"), away_team: Team.by_name("Urugwaj"), 
			start_date: Time.new(2018, 6, 15,  14,  0,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion Centralny", city: "Jekaterynburg")
		
		Game.create!(home_team: Team.by_name("Rosja"), away_team: Team.by_name("Egipt"), 
			start_date: Time.new(2018, 6, 19,  20,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stadion Kriestowskij", city: "Sankt Petersburg")
		Game.create!(home_team: Team.by_name("Urugwaj"), away_team: Team.by_name("Arabia Saudyjska"), 
			start_date: Time.new(2018, 6, 20,  17,  0,  0), event_id: @@event.id, game_group: "Grupa A", location: "Rostów Arena", city: "Rostów")
		
		Game.create!(home_team: Team.by_name("Arabia Saudyjska"), away_team: Team.by_name("Egipt"), 
			start_date: Time.new(2018, 6, 25,  16,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Wołgograd Arena", city: "Wołgograd")
		Game.create!(home_team: Team.by_name("Urugwaj"), away_team: Team.by_name("Rosja"), 
			start_date: Time.new(2018, 6, 25,  16,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Samara Arena", city: "Samara")

		#Gr. B
		Game.create!(home_team: Team.by_name("Maroko"), away_team: Team.by_name("Iran"), 
			start_date: Time.new(2018, 6, 15,  17,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion Kriestowskij", city: "Sankt Petersburg")
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Hiszpania"), 
			start_date: Time.new(2018, 6, 15,  20,  0,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion Olimpijski", city: "Soczi")
		
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Maroko"), 
			start_date: Time.new(2018, 6, 20,  14,  0,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion Łużniki", city: "Moskwa")
		Game.create!(home_team: Team.by_name("Iran"), away_team: Team.by_name("Hiszpania"), 
			start_date: Time.new(2018, 6, 20,  20,  0,  0), event_id: @@event.id, game_group: "Grupa B", location: "Kazań Arena", city: "Kazań")
		
		Game.create!(home_team: Team.by_name("Iran"), away_team: Team.by_name("Portugalia"), 
			start_date: Time.new(2018, 6, 25,  20,  0,  0), event_id: @@event.id, game_group: "Grupa B", location: "Mordowia Arena", city: "Sarańsk")
		Game.create!(home_team: Team.by_name("Hiszpania"), away_team: Team.by_name("Maroko"), 
			start_date: Time.new(2018, 6, 25,  20,  0,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadion Kaliningrad", city: "Kaliningrad")

		#Gr. C
		Game.create!(home_team: Team.by_name("Francja"), away_team: Team.by_name("Australia"), 
			start_date: Time.new(2018, 6, 16,  12,  0,  0), event_id: @@event.id, game_group: "Grupa C", location: "Kazań Arena", city: "Kazań")
		Game.create!(home_team: Team.by_name("Peru"), away_team: Team.by_name("Dania"), 
			start_date: Time.new(2018, 6, 16,  18,  0,  0), event_id: @@event.id, game_group: "Grupa C", location: "Mordowia Arena", city: "Sarańsk")
		
		Game.create!(home_team: Team.by_name("Dania"), away_team: Team.by_name("Australia"), 
			start_date: Time.new(2018, 6, 21,  14,  0,  0), event_id: @@event.id, game_group: "Grupa C", location: "Samara Arena", city: "Samara")
		Game.create!(home_team: Team.by_name("Francja"), away_team: Team.by_name("Peru"), 
			start_date: Time.new(2018, 6, 21,  17,  0,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion Centralny", city: "Jekaterynburg")
		
		Game.create!(home_team: Team.by_name("Australia"), away_team: Team.by_name("Peru"), 
			start_date: Time.new(2018, 6, 26,  16,  0,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion Olimpijski", city: "Soczi")
		Game.create!(home_team: Team.by_name("Dania"), away_team: Team.by_name("Francja"), 
			start_date: Time.new(2018, 6, 26,  16,  0,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stadion Łużniki", city: "Moskwa")

		#Gr. D
		Game.create!(home_team: Team.by_name("Argentyna"), away_team: Team.by_name("Islandia"), 
			start_date: Time.new(2018, 6, 16,  15,  0,  0), event_id: @@event.id, game_group: "Grupa D", location: "Otkrytije Ariena", city: "Moskwa")
		Game.create!(home_team: Team.by_name("Chorwacja"), away_team: Team.by_name("Nigeria"), 
			start_date: Time.new(2018, 6, 16,  21,  0,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadion Kaliningrad", city: "Kaliningrad")
		
		Game.create!(home_team: Team.by_name("Argentyna"), away_team: Team.by_name("Chorwacja"), 
			start_date: Time.new(2018, 6, 21,  20,  0,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadion Niżny Nowogród", city: "Niżny Nowogród")
		Game.create!(home_team: Team.by_name("Nigeria"), away_team: Team.by_name("Islandia"), 
			start_date: Time.new(2018, 6, 22,  17,  0,  0), event_id: @@event.id, game_group: "Grupa D", location: "Wołgograd Arena", city: "Wołgograd")
		
		Game.create!(home_team: Team.by_name("Nigeria"), away_team: Team.by_name("Argentyna"), 
			start_date: Time.new(2018, 6, 26,  20,  0,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadion Kriestowskij", city: "Sankt Petersburg")
		Game.create!(home_team: Team.by_name("Islandia"), away_team: Team.by_name("Chorwacja"), 
			start_date: Time.new(2018, 6, 26,  20,  0,  0), event_id: @@event.id, game_group: "Grupa D", location: "Rostów Arena", city: "Rostów")

		#Gr. E
		Game.create!(home_team: Team.by_name("Kostaryka"), away_team: Team.by_name("Serbia"), 
			start_date: Time.new(2018, 6, 17,  14,  0,  0), event_id: @@event.id, game_group: "Grupa E", location: "Samara Arena", city: "Samara")
		Game.create!(home_team: Team.by_name("Brazylia"), away_team: Team.by_name("Szwajcaria"), 
			start_date: Time.new(2018, 6, 17,  20,  0,  0), event_id: @@event.id, game_group: "Grupa E", location: "Rostów Arena", city: "Rostów")
		
		Game.create!(home_team: Team.by_name("Brazylia"), away_team: Team.by_name("Kostaryka"), 
			start_date: Time.new(2018, 6, 22,  14,  0,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stadion Kriestowskij", city: "Sankt Petersburg")
		Game.create!(home_team: Team.by_name("Serbia"), away_team: Team.by_name("Szwajcaria"), 
			start_date: Time.new(2018, 6, 22,  20,  0,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stadion Kaliningrad", city: "Kaliningrad")
		
		Game.create!(home_team: Team.by_name("Serbia"), away_team: Team.by_name("Brazylia"), 
			start_date: Time.new(2018, 6, 27,  20,  0,  0), event_id: @@event.id, game_group: "Grupa E", location: "Otkrytije Ariena", city: "Moskwa")
		Game.create!(home_team: Team.by_name("Szwajcaria"), away_team: Team.by_name("Kostaryka"), 
			start_date: Time.new(2018, 6, 27,  20,  0,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stadion Niżny Nowogród", city: "Niżny Nowogród")

		#Gr. F
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Meksyk"), 
			start_date: Time.new(2018, 6, 17,  17,  0,  0), event_id: @@event.id, game_group: "Grupa F", location: "Otkrytije Ariena", city: "Moskwa")
		Game.create!(home_team: Team.by_name("Szwecja"), away_team: Team.by_name("Korea Południowa"), 
			start_date: Time.new(2018, 6, 18,  14,  0,  0), event_id: @@event.id, game_group: "Grupa F", location: "Stadion Niżny Nowogród", city: "Niżny Nowogród")
		
		Game.create!(home_team: Team.by_name("Korea Południowa"), away_team: Team.by_name("Meksyk"), 
			start_date: Time.new(2018, 6, 23,  17,  0,  0), event_id: @@event.id, game_group: "Grupa F", location: "Rostów Arena", city: "Rostów")
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Szwecja"), 
			start_date: Time.new(2018, 6, 23,  20,  0,  0), event_id: @@event.id, game_group: "Grupa F", location: "Stadion Olimpijski", city: "Soczi")
		
		Game.create!(home_team: Team.by_name("Korea Południowa"), away_team: Team.by_name("Niemcy"), 
			start_date: Time.new(2018, 6, 27,  16,  0,  0), event_id: @@event.id, game_group: "Grupa F", location: "Kazań Arena", city: "Kazań")
		Game.create!(home_team: Team.by_name("Meksyk"), away_team: Team.by_name("Szwecja"), 
			start_date: Time.new(2018, 6, 27,  16,  0,  0), event_id: @@event.id, game_group: "Grupa F", location: "Stadion Centralny", city: "Jekaterynburg")

		#Gr. G
		Game.create!(home_team: Team.by_name("Belgia"), away_team: Team.by_name("Panama"), 
			start_date: Time.new(2018, 6, 18,  17,  0,  0), event_id: @@event.id, game_group: "Grupa G", location: "Stadion Olimpijski", city: "Soczi")
		Game.create!(home_team: Team.by_name("Tunezja"), away_team: Team.by_name("Anglia"), 
			start_date: Time.new(2018, 6, 18,  20,  0,  0), event_id: @@event.id, game_group: "Grupa G", location: "Wołgograd Arena", city: "Wołgograd")
		
		Game.create!(home_team: Team.by_name("Belgia"), away_team: Team.by_name("Tunezja"), 
			start_date: Time.new(2018, 6, 23,  14,  0,  0), event_id: @@event.id, game_group: "Grupa G", location: "Otkrytije Ariena", city: "Moskwa")
		Game.create!(home_team: Team.by_name("Anglia"), away_team: Team.by_name("Panama"), 
			start_date: Time.new(2018, 6, 24,  14,  0,  0), event_id: @@event.id, game_group: "Grupa G", location: "Stadion Niżny Nowogród", city: "Niżny Nowogród")
		
		Game.create!(home_team: Team.by_name("Anglia"), away_team: Team.by_name("Belgia"), 
			start_date: Time.new(2018, 6, 28,  20,  0,  0), event_id: @@event.id, game_group: "Grupa G", location: "Stadion Kaliningrad", city: "Kaliningrad")
		Game.create!(home_team: Team.by_name("Panama"), away_team: Team.by_name("Tunezja"), 
			start_date: Time.new(2018, 6, 28,  20,  0,  0), event_id: @@event.id, game_group: "Grupa G", location: "Mordowia Arena", city: "Sarańsk")
		
		#Gr. H
		Game.create!(home_team: Team.by_name("Kolumbia"), away_team: Team.by_name("Japonia"), 
			start_date: Time.new(2018, 6, 19,  14,  0,  0), event_id: @@event.id, game_group: "Grupa H", location: "Mordowia Arena", city: "Sarańsk")
		Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Senegal"), 
			start_date: Time.new(2018, 6, 19,  17,  0,  0), event_id: @@event.id, game_group: "Grupa H", location: "Otkrytije Ariena", city: "Moskwa")
		
		Game.create!(home_team: Team.by_name("Japonia"), away_team: Team.by_name("Senegal"), 
			start_date: Time.new(2018, 6, 24,  17,  0,  0), event_id: @@event.id, game_group: "Grupa H", location: "Stadion Centralny", city: "Jekaterynburg")
		Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Kolumbia"), 
			start_date: Time.new(2018, 6, 24,  20,  0,  0), event_id: @@event.id, game_group: "Grupa H", location: "Kazań Arena", city: "Kazań")
		
		Game.create!(home_team: Team.by_name("Japonia"), away_team: Team.by_name("Polska"), 
			start_date: Time.new(2018, 6, 28,  16,  0,  0), event_id: @@event.id, game_group: "Grupa H", location: "Wołgograd Arena", city: "Wołgograd")
		Game.create!(home_team: Team.by_name("Senegal"), away_team: Team.by_name("Kolumbia"), 
			start_date: Time.new(2018, 6, 28,  16,  0,  0), event_id: @@event.id, game_group: "Grupa H", location: "Samara Arena", city: "Samara")

		#1/8
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 6, 30,  16,  0,  0), event_id: @@event.id, game_group: "1/8", location: "Stadion Olimpijski", city: "Soczi")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 6, 30,  20,  0,  0), event_id: @@event.id, game_group: "1/8", location: "Kazań Arena", city: "Kazań")
	
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 1,  16,  0,  0), event_id: @@event.id, game_group: "1/8", location: "Łuzniki", city: "Moskwa")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 1,  20,  0,  0), event_id: @@event.id, game_group: "1/8", location: "Stadion Niżny Nowogród", city: "Niżny Nowogród")
		
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 2,  16,  0,  0), event_id: @@event.id, game_group: "1/8", location: "Samara Arena", city: "Samara")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 2,  20,  0,  0), event_id: @@event.id, game_group: "1/8", location: "Rostów Arena", city: "Rostów")

		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 3,  16,  0,  0), event_id: @@event.id, game_group: "1/8", location: "Stadion Kriestowskij", city: "Sankt Petersburg")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 3,  20,  0,  0), event_id: @@event.id, game_group: "1/8", location: "Otkrytije Ariena", city: "Moskwa")

		#quaters

		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 6,  16,  0,  0), event_id: @@event.id, game_group: "1/4", location: "Stadion Niżny Nowogród", city: "Niżny Nowogród")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 6,  20,  0,  0), event_id: @@event.id, game_group: "1/4", location: "Kazań Arena", city: "Kazań")

		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 7,  16,  0,  0), event_id: @@event.id, game_group: "1/4", location: "Stadion Olimpijski", city: "Soczi")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 7,  20,  0,  0), event_id: @@event.id, game_group: "1/4", location: "Samara Arena", city: "Samara")

		#semis
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 10,  20,  0,  0), event_id: @@event.id, game_group: "Półfinał", location: "Stadion Kriestowskij", city: "Sankt Petersburg")
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 11,  20,  0,  0), event_id: @@event.id, game_group: "Półfinał", location: "Łuzniki", city: "Moskwa")

		#o 3 miejsce
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 14,  16,  0,  0), event_id: @@event.id, game_group: "O 3 miejsce", location: "Stadion Kriestowskij", city: "Sankt Petersburg")
	
		#final
		Game.create!(home_team: nil, away_team: nil,
			start_date: Time.new(2018, 7, 15,  17,  0,  0), event_id: @@event.id, game_group: "Finał", location: "Łuzniki", city: "Moskwa")
	

	end
end
