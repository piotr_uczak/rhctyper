class EURO2016Seeds
	@@event

	def self.SeedEvent
		@@event = Event.create!(name: "Euro 2016")
	end

	def self.event
		@@event
	end

	def self.SeedTeams
		Team.create!(name: "Polska", national:true, flag_file_name: "300px-flaga-polski.png",  flag_content_type: "image/png", flag_file_size: 537, flag_updated_at: "2015-07-11 19:51:42")
		Team.create!(name: "Niemcy", national:true, flag_file_name: "Flaga_Niemiec.png", flag_content_type: "image/png", flag_file_size: 1929, flag_updated_at: "2015-07-11 19:53:49")
		Team.create!(name: "Anglia", national:true, flag_file_name: "Flaga_Anglii.png", flag_content_type: "image/png", flag_file_size: 1514, flag_updated_at: "2015-07-11 19:54:42")
		Team.create!(name: "Albania", national:true, flag_file_name: "albania.jpg", flag_content_type: "image/jpeg")
		Team.create!(name: "Austria", national:true, flag_file_name: "at.png", flag_content_type: "image/png")
		Team.create!(name: "Belgia", national:true, flag_file_name: "be.png", flag_content_type: "image/png")
		Team.create!(name: "Chorwacja", national:true, flag_file_name: "hr.png", flag_content_type: "image/png")
		Team.create!(name: "Czechy", national:true, flag_file_name: "cz.png", flag_content_type: "image/png")
		Team.create!(name: "Francja", national:true, flag_file_name: "fr.png", flag_content_type: "image/png")
		Team.create!(name: "Węgry", national:true, flag_file_name: "hu.png", flag_content_type: "image/png")
		Team.create!(name: "Islandia", national:true, flag_file_name: "is.png", flag_content_type: "image/png")
		Team.create!(name: "Irlandia Płn.", national:true, flag_file_name: "ni.png", flag_content_type: "image/png")
		Team.create!(name: "Irlandia", national:true, flag_file_name: "ie.png", flag_content_type: "image/png")
		Team.create!(name: "Portugalia", national:true, flag_file_name: "pt.png", flag_content_type: "image/png")
		Team.create!(name: "Rumunia", national:true, flag_file_name: "ro.png", flag_content_type: "image/png")
		Team.create!(name: "Rosja", national:true, flag_file_name: "ru.png", flag_content_type: "image/png")
		Team.create!(name: "Słowacja", national:true, flag_file_name: "sk.png", flag_content_type: "image/png")
		Team.create!(name: "Hiszpania", national:true, flag_file_name: "es.png", flag_content_type: "image/png")
		Team.create!(name: "Szwecja", national:true, flag_file_name: "se.png", flag_content_type: "image/png")
		Team.create!(name: "Turcja", national:true, flag_file_name: "tr.png", flag_content_type: "image/png")
		Team.create!(name: "Szwajcaria", national:true, flag_file_name: "ch.png", flag_content_type: "image/png")
		Team.create!(name: "Walia", national:true, flag_file_name: "wl.png", flag_content_type: "image/png")
		Team.create!(name: "Ukraina", national:true, flag_file_name: "ua.png", flag_content_type: "image/png")
		Team.create!(name: "Włochy", national:true, flag_file_name: "it.png", flag_content_type: "image/png")
		#Team.create!(name: "-",national:true)
	end

	def self.SeedMatches
#1 runda
	#10.06
		Game.create!(home_team: Team.by_name("Francja"), away_team: Team.by_name("Rumunia"), 
			start_date: Time.new(2016, 6, 10,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stade de France", city: "Saint-Denis")
	#11.06
		Game.create!(home_team: Team.by_name("Albania"), away_team: Team.by_name("Szwajcaria"), 
			start_date: Time.new(2016, 6, 11,  15,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stade Bollaert-Delelis", city: "Lens")
		Game.create!(home_team: Team.by_name("Walia"), away_team: Team.by_name("Słowacja"), 
			start_date: Time.new(2016, 6, 11,  18,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Nouveau Stade de Bordeaux", city: "Bordeaux")
		Game.create!(home_team: Team.by_name("Anglia"), away_team: Team.by_name("Rosja"), 
			start_date: Time.new(2016, 6, 11,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stade Vélodrome", city: "Marseille")
	#12.06	
		Game.create!(home_team: Team.by_name("Turcja"), away_team: Team.by_name("Chorwacja"), 
			start_date: Time.new(2016, 6, 12,  15,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Parc des Princes", city: "Paris")
		Game.create!(home_team: Team.by_name("Polska"), away_team: Team.by_name("Irlandia Płn."), 
			start_date: Time.new(2016, 6, 12,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Allianz Riviera", city: "Nice")
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Ukraina"), 
			start_date: Time.new(2016, 6, 12,  21,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Grand Stade Lille Métropole", city: "Lille")
	#13.06	
		Game.create!(home_team: Team.by_name("Hiszpania"), away_team: Team.by_name("Czechy"), 
			start_date: Time.new(2016, 6, 13,  15,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stadium Municipal", city: "Toulouse")
		Game.create!(home_team: Team.by_name("Irlandia"), away_team: Team.by_name("Szwecja"), 
			start_date: Time.new(2016, 6, 13,  18,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stade de France", city: "Saint-Denis")
		Game.create!(home_team: Team.by_name("Belgia"), away_team: Team.by_name("Włochy"), 
			start_date: Time.new(2016, 6, 13,  21,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stade de Lyon", city: "Lyon")
	#14.06	
		Game.create!(home_team: Team.by_name("Austria"), away_team: Team.by_name("Węgry"), 
			start_date: Time.new(2016, 6, 14,  18,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Nouveau Stade de Bordeaux", city: "Bordeaux")
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Islandia"), 
			start_date: Time.new(2016, 6, 14,  21,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Geoffroy-Guichard", city: "Saint-Etienne")
#2 runda
	#15.06
		Game.create!(home_team: Team.by_name("Rosja"), away_team: Team.by_name("Słowacja"), 
			start_date: Time.new(2016, 6, 15,  15,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Grand Stade Lille Métropole", city: "Lille")
		Game.create!(home_team: Team.by_name("Rumunia"), away_team: Team.by_name("Szwajcaria"), 
			start_date: Time.new(2016, 6, 15,  18,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Parc des Princes", city: "Paris")
		Game.create!(home_team: Team.by_name("Francja"), away_team: Team.by_name("Albania"), 
			start_date: Time.new(2016, 6, 15,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stade Vélodrome", city: "Marseille")
	#16.06
		Game.create!(home_team: Team.by_name("Anglia"), away_team: Team.by_name("Walia"), 
			start_date: Time.new(2016, 6, 16,  15,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stade Bollaert-Delelis", city: "Lens")
		Game.create!(home_team: Team.by_name("Ukraina"), away_team: Team.by_name("Irlandia Płn."), 
			start_date: Time.new(2016, 6, 16,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stade de Lyon", city: "Lyon")
		Game.create!(home_team: Team.by_name("Niemcy"), away_team: Team.by_name("Polska"), 
			start_date: Time.new(2016, 6, 16,  21,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stade de France", city: "Saint-Denis")
	#17.06
		Game.create!(home_team: Team.by_name("Włochy"), away_team: Team.by_name("Szwecja"), 
			start_date: Time.new(2016, 6, 17,  15,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Stadium Municipal", city: "Toulouse")
		Game.create!(home_team: Team.by_name("Czechy"), away_team: Team.by_name("Chorwacja"), 
			start_date: Time.new(2016, 6, 17,  18,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Geoffroy-Guichard", city: "Saint-Etienne")
		Game.create!(home_team: Team.by_name("Hiszpania"), away_team: Team.by_name("Turcja"), 
			start_date: Time.new(2016, 6, 17,  21,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Allianz Riviera", city: "Nice")
	#18.06
		Game.create!(home_team: Team.by_name("Belgia"), away_team: Team.by_name("Irlandia"), 
			start_date: Time.new(2016, 6, 18,  15,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Nouveau Stade de Bordeaux", city: "Bordeaux")
		Game.create!(home_team: Team.by_name("Islandia"), away_team: Team.by_name("Węgry"), 
			start_date: Time.new(2016, 6, 18,  18,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Stade Vélodrome", city: "Marseille")
		Game.create!(home_team: Team.by_name("Portugalia"), away_team: Team.by_name("Austria"), 
			start_date: Time.new(2016, 6, 18,  21,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Parc des Princes", city: "Paris")
#3 runda
	#19.06
		Game.create!(home_team: Team.by_name("Szwajcaria"), away_team: Team.by_name("Francja"), 
			start_date: Time.new(2016, 6, 19,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Grand Stade Lille Métropole", city: "Lille")
		Game.create!(home_team: Team.by_name("Rumunia"), away_team: Team.by_name("Albania"), 
			start_date: Time.new(2016, 6, 19,  21,  00,  0), event_id: @@event.id, game_group: "Grupa A", location: "Stade de Lyon", city: "Lyon")
	#20.06
		Game.create!(home_team: Team.by_name("Słowacja"), away_team: Team.by_name("Anglia"), 
			start_date: Time.new(2016, 6, 20,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Geoffroy-Guichard", city: "Saint-Etienne")
		Game.create!(home_team: Team.by_name("Rosja"), away_team: Team.by_name("Walia"), 
			start_date: Time.new(2016, 6, 20,  21,  00,  0), event_id: @@event.id, game_group: "Grupa B", location: "Stadium Municipal", city: "Toulouse")
	#21.06
		Game.create!(home_team: Team.by_name("Irlandia Płn."), away_team: Team.by_name("Niemcy"), 
			start_date: Time.new(2016, 6, 21,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Parc des Princes", city: "Paris")
		Game.create!(home_team: Team.by_name("Ukraina"), away_team: Team.by_name("Polska"), 
			start_date: Time.new(2016, 6, 21,  18,  00,  0), event_id: @@event.id, game_group: "Grupa C", location: "Stade Vélodrome", city: "Marseille")
		Game.create!(home_team: Team.by_name("Chorwacja"), away_team: Team.by_name("Hiszpania"), 
			start_date: Time.new(2016, 6, 21,  21,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Nouveau Stade de Bordeaux", city: "Bordeaux")
		Game.create!(home_team: Team.by_name("Czechy"), away_team: Team.by_name("Turcja"), 
			start_date: Time.new(2016, 6, 21,  21,  00,  0), event_id: @@event.id, game_group: "Grupa D", location: "Stade Bollaert-Delelis", city: "Lens")
	#22.06
		Game.create!(home_team: Team.by_name("Islandia"), away_team: Team.by_name("Austria"), 
			start_date: Time.new(2016, 6, 22,  18,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Stade de France", city: "Saint-Denis")
		Game.create!(home_team: Team.by_name("Węgry"), away_team: Team.by_name("Portugalia"), 
			start_date: Time.new(2016, 6, 22,  18,  00,  0), event_id: @@event.id, game_group: "Grupa F", location: "Stade de Lyon", city: "Lyon")
		Game.create!(home_team: Team.by_name("Szwecja"), away_team: Team.by_name("Belgia"), 
			start_date: Time.new(2016, 6, 22,  21,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Allianz Riviera", city: "Nice")
		Game.create!(home_team: Team.by_name("Włochy"), away_team: Team.by_name("Irlandia"), 
			start_date: Time.new(2016, 6, 22,  21,  00,  0), event_id: @@event.id, game_group: "Grupa E", location: "Grand Stade Lille Métropole", city: "Lille")	

#szesnastka
	#25.06
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 25,  15,  00,  0), event_id: @@event.id, game_group: "1/16 finału", location: "Geoffroy-Guichard", city: "Saint-Etienne")
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 25,  18,  00,  0), event_id: @@event.id, game_group: "1/16 finału", location: "Parc des Princes", city: "Paris")
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 25,  21,  00,  0), event_id: @@event.id, game_group: "1/16 finału", location: "Stade Bollaert-Delelis", city: "Lens")
	#26.06
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 26,  15,  00,  0), event_id: @@event.id, game_group: "1/16 finału", location: "Stade de Lyon", city: "Lyon")
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 26,  18,  00,  0), event_id: @@event.id, game_group: "1/16 finału", location: "Grand Stade Lille Métropole", city: "Lille")
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 26,  21,  00,  0), event_id: @@event.id, game_group: "1/16 finału", location: "Stadium Municipal", city: "Toulouse")
	#27.06
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 27,  18,  00,  0), event_id: @@event.id, game_group: "1/16 finału", location: "Stade de France", city: "Saint-Denis")
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 27,  21,  00,  0), event_id: @@event.id, game_group: "1/16 finału", location: "Allianz Riviera", city: "Nice")

#ćwierćfinały
	#30.06
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 6, 30,  21,  00,  0), event_id: @@event.id, game_group: "I ćwierćfinał", location: "Stade Vélodrome", city: "Marseille")

	#01.07
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 7, 1,  21,  00,  0), event_id: @@event.id, game_group: "II ćwierćfinał", location: "Grand Stade Lille Métropole", city: "Lille")

	#02.06
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 7, 2,  21,  00,  0), event_id: @@event.id, game_group: "III ćwierćfinał", location: "Nouveau Stade de Bordeaux", city: "Bordeaux")

	#03.07
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 7, 3,  21,  00,  0), event_id: @@event.id, game_group: "IV ćwierćfinał", location: "Stade de France", city: "Saint-Denis")

#półfinały
	#06.07
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 7, 6,  21,  00,  0), event_id: @@event.id, game_group: "I półfinał", location: "Stade de Lyon", city: "Lyon")

	#07.07
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 7, 7,  21,  00,  0), event_id: @@event.id, game_group: "II półfinał", location: "Stade Vélodrome", city: "Marseille")

#finał
	#10.07
		Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 7, 10,  21,  00,  0), event_id: @@event.id, game_group: "FINAŁ", location: "Stade de France", city: "Saint-Denis")

	end
end