class CL2016Seeds
	@@event

	def self.SeedEvent
		@@event = Event.create!(name: "Liga Mistrzów 2016")
	end

	def self.event
		@@event
	end

	def self.SeedTeams
		Team.create!(name: "FC Barcelona", national:false, flag_file_name: "bar.png", flag_content_type: "image/png")
		Team.create!(name: "Real Madryt", national:false, flag_file_name: "rea.png", flag_content_type: "image/png")
		Team.create!(name: "Bayern M.", national:false, flag_file_name: "bay.png", flag_content_type: "image/png")
		Team.create!(name: "Atletico Madryt", national:false, flag_file_name: "atl.png", flag_content_type: "image/png")
		Team.create!(name: "Vfl Wolfsburg", national:false, flag_file_name: "vfl.png", flag_content_type: "image/png")
		Team.create!(name: "Benfica Lizbona", national:false, flag_file_name: "ben.png", flag_content_type: "image/png")
		Team.create!(name: "Paris St Germain", national:false, flag_file_name: "psg.png", flag_content_type: "image/png")
		Team.create!(name: "Manchester City", national:false, flag_file_name: "mci.png", flag_content_type: "image/png")
	end

	def self.SeedMatches
Game.create!(home_team: Team.by_name("FC Barcelona"), away_team: Team.by_name("Atletico Madryt"), 
			start_date: Time.new(2016, 4, 5,  20,  45,  0), event_id: @@event.id, game_group: "Ćwierćfinał, I mecz", location: "Camp Nou", city: "Barcelona")
Game.create!(home_team: Team.by_name("Atletico Madryt"), away_team: Team.by_name("FC Barcelona"), 
			start_date: Time.new(2016, 4, 13,  20,  45,  0), event_id: @@event.id, game_group: "Ćwierćfinał, rewanż", location: "Vicente Calderón", city: "Madryt")
Game.create!(home_team: Team.by_name("Bayern M."), away_team: Team.by_name("Benfica Lizbona"), 
			start_date: Time.new(2016, 4, 5,  20,  45,  0), event_id: @@event.id, game_group: "Ćwierćfinał, I mecz", location: "Allianz Arena", city: "Monachium")
Game.create!(home_team: Team.by_name("Benfica Lizbona"), away_team: Team.by_name("Bayern M."), 
			start_date: Time.new(2016, 4, 13,  20,  45,  0), event_id: @@event.id, game_group: "Ćwierćfinał, rewanż", location: "Estádio da Luz", city: "Lizbona")
Game.create!(home_team: Team.by_name("Paris St Germain"), away_team: Team.by_name("Manchester City"), 
			start_date: Time.new(2016, 4, 6,  20,  45,  0), event_id: @@event.id, game_group: "Ćwierćfinał, I mecz", location: "Parc des Princes", city: "Paryż")
Game.create!(home_team: Team.by_name("Manchester City"), away_team: Team.by_name("Paris St Germain"), 
			start_date: Time.new(2016, 4, 12,  20,  45,  0), event_id: @@event.id, game_group: "Ćwierćfinał, rewanż", location: "Etihad Stadium", city: "Manchester")
Game.create!(home_team: Team.by_name("Vfl Wolfsburg"), away_team: Team.by_name("Real Madryt"), 
			start_date: Time.new(2016, 4, 6,  20,  45,  0), event_id: @@event.id, game_group: "Ćwierćfinał, I mecz", location: "Volkswagen Arena", city: "Wolfsburg")
Game.create!(home_team: Team.by_name("Real Madryt"), away_team: Team.by_name("Vfl Wolfsburg"), 
			start_date: Time.new(2016, 4, 12,  20,  45,  0), event_id: @@event.id, game_group: "Ćwierćfinał, rewanż", location: "Estadio Santiago Bernabeu", city: "Madryt")
#Półfinały
Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 4, 26,  20,  45,  0), event_id: @@event.id, game_group: "I Półfinał, I mecz")
Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 4, 27,  20,  45,  0), event_id: @@event.id, game_group: "II Półfinał, I mecz")
Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 5, 3,  20,  45,  0), event_id: @@event.id, game_group: "II Półfinał, rewanż")
Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 5, 4,  20,  45,  0), event_id: @@event.id, game_group: "I Półfinał, rewanż")
#Finał
Game.create!(home_team: nil, away_team: nil, 
			start_date: Time.new(2016, 5, 28,  20,  45,  0), event_id: @@event.id, game_group: "Finał", location: "Estadio Giuseppe Meazza", city: "Mediolan")
	end
end