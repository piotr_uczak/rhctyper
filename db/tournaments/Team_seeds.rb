class TeamSeeds

	def self.SeedTeams
		Team.create!(name: "Polska", national:true, flag_file_name: "300px-flaga-polski.png",  flag_content_type: "image/png")
		Team.create!(name: "Niemcy", national:true, flag_file_name: "Flaga_Niemiec.png", flag_content_type: "image/png")
		Team.create!(name: "Anglia", national:true, flag_file_name: "Flaga_Anglii.png", flag_content_type: "image/png")
		Team.create!(name: "Albania", national:true, flag_file_name: "albania.jpg", flag_content_type: "image/png")
		Team.create!(name: "Austria", national:true, flag_file_name: "at.png", flag_content_type: "image/png")
		Team.create!(name: "Ukraina", national:true, flag_file_name: "ua.png", flag_content_type: "image/png")
		Team.create!(name: "Szwajcaria", national:true, flag_file_name: "ch.png", flag_content_type: "image/png")
		Team.create!(name: "Chorwacja", national:true, flag_file_name: "hr.png", flag_content_type: "image/png")
		Team.create!(name: "Hiszpania", national:true, flag_file_name: "es.png", flag_content_type: "image/png")
		Team.create!(name: "Francja", national:true, flag_file_name: "fr.png", flag_content_type: "image/png")
		Team.create!(name: "Belgia", national:true, flag_file_name: "be.png", flag_content_type: "image/png")
		Team.create!(name: "Włochy", national:true, flag_file_name: "it.png", flag_content_type: "image/png")
		Team.create!(name: "Czechy", national:true, flag_file_name: "cz.png", flag_content_type: "image/png")
		Team.create!(name: "Portugalia", national:true, flag_file_name: "pt.png", flag_content_type: "image/png")
		Team.create!(name: "Walia", national:true, flag_file_name: "wl.png", flag_content_type: "image/png")
		Team.create!(name: "Szwecja", national:true, flag_file_name: "se.png", flag_content_type: "image/png")
		Team.create!(name: "Turcja", national:true, flag_file_name: "tr.png", flag_content_type: "image/png")
		Team.create!(name: "Węgry", national:true, flag_file_name: "hu.png", flag_content_type: "image/png")
		Team.create!(name: "Słowacja", national:true, flag_file_name: "sk.png", flag_content_type: "image/png")
		Team.create!(name: "Dania", national:true, flag_file_name: "dania.png", flag_content_type: "image/png")
		Team.create!(name: "Holandia", national:true, flag_file_name: "nl.png", flag_content_type: "image/png")
		Team.create!(name: "Szkocja", national:true, flag_file_name: "sc.png", flag_content_type: "image/png")
		Team.create!(name: "Słowenia", national:true, flag_file_name: "slo.png", flag_content_type: "image/png")
		Team.create!(name: "Serbia", national:true, flag_file_name: "serbia.png", flag_content_type: "image/png")
		Team.create!(name: "Rumunia", national:true, flag_file_name: "ro.png", flag_content_type: "image/png")
		Team.create!(name: "Gruzja", national:true, flag_file_name: "geo.png", flag_content_type: "image/png")
    #Team.create!(name: "-",national:true)
	end

end