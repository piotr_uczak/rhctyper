# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

%w{
  EURO_2024_seeds
}.each do |part|
  require File.expand_path(File.dirname(__FILE__))+"/tournaments/#{part}.rb"
end

  # TeamSeeds.SeedTeams()
  EURO2024Seeds.SeedEvent()
  EURO2024Seeds.SeedMatches()
  # EURO2020Seeds.SeedEvent()
  # EURO2020Seeds.SeedTeams()
  # EURO2020Seeds.SeedMatches()
  #WC2018Seeds.SeedEvent()
  #WC2018Seeds.DeleteAllMatches()
  #WC2018Seeds.SeedMatches()

# CL2016Seeds.SeedEvent()
# CL2016Seeds.SeedTeams()
# CL2016Seeds.SeedMatches()

# Friendly2016Seeds.SeedEvent()
# Friendly2016Seeds.SeedTeams()
# Friendly2016Seeds.SeedMatches()

#EURO2017Seeds.SeedEvent()
#EURO2017Seeds.SeedTeams()
#EURO2017Seeds.SeedMatches()

# user = CreateAdminService.new.call
# puts 'CREATED ADMIN USER: ' << user.email

# @u1 = User.new
# @u1.name = "Michał Matuszek"
# @u1.email = "michal.a.matuszek@gmail.com"
# @u1.password = "12345678"
# @u1.password_confirmation = "12345678"
# @u1.save!
# @u1.skip_confirmation!
# @u1.admin!

# @u2 = User.new
# @u2.name = "Andrzej Łuczak"
# @u2.email = "lkd.lex@poczta.interia.pl"
# @u2.password = "12345678"
# @u2.password_confirmation = "12345678"
# @u2.save!
# @u2.skip_confirmation!

# @u3 = User.new
# @u3.name = "Artur Bator"
# @u3.email = "artur.tychy@op.pl"
# @u3.password = "12345678"
# @u3.password_confirmation = "12345678"
# @u3.save!
# @u3.skip_confirmation!


# @u4 = User.new
# @u4.name = "Andrzej Matuszek"
# @u4.email = "amatuszek@onet.eu"
# @u4.password = "12345678"
# @u4.password_confirmation = "12345678"
# @u4.save!
# @u4.skip_confirmation!

# @u5 = User.new
# @u5.name = "Anna Łuczak"
# @u5.email = "anna.e.luczak@gmail.com"
# @u5.password = "12345678"
# @u5.password_confirmation = "12345678"
# @u5.save!
# @u5.skip_confirmation!

# Competition.create!(event: EURO2016Seeds.event, creator_id: 1) 
# CompetitionsUsers.create!(user_id: 1, competition_id: 1)
# CompetitionsUsers.create!(user: @u1, competition_id: 1)
# CompetitionsUsers.create!(user: @u2, competition_id: 1)

#Bet.create!(game_id: 1, competition_id: 1, owner_id: 1, home_score: 0, away_score: 0, points_earned: 0, is_won: false)
#Bet.create!(game_id: 1, competition_id: 1, owner: @u1, home_score: 0, away_score: 1, points_earned: 3, is_won: true)
#Bet.create!(game_id: 1, competition_id: 1, owner: @u2, home_score: 0, away_score: 1, points_earned: 3, is_won: true)
#Bet.create!(game_id: 2, competition_id: 1, owner: @u1, home_score: 2, away_score: 2, points_earned: 1, is_won: true)
#Bet.create!(game_id: 2, competition_id: 1, owner: @u2, home_score: 2, away_score: 1, points_earned: 0, is_won: false)
